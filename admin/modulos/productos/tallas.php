<?php
// BREADCRUMB
	echo '
	<div class="uk-width-auto margin-top-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=tallas" class="color-red">Tallas</a></li>
		</ul>
	</div>';


// BOTONES SUPERIORES
	echo '
	<div class="uk-width-expand@m margin-v-20">
		<div uk-grid class="uk-grid-small uk-flex-right">
			<div>
				<a href="#agregar" uk-toggle class="uk-button uk-button-success"><i uk-icon="plus"></i> &nbsp; Nuevo</a>
			</div>
		</div>
	</div>';


// TABLA DE CATEGORÍAS
	echo '
	<div class="uk-width-1-1 margin-v-20">
		<div class="uk-container">
			<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle" id="ordenar">
				<thead>
					<tr class="uk-text-muted">
						<th onclick="sortTable(0)" class="pointer uk-text-left">Tallas</th>
						<th width="120px" ></th>
					</tr>
				</thead>
				<tbody class="sortable" data-tabla="'.$modulocat.'">';

					// Obtener subcategorías
					$numeroProds=0;
					$subcatsNum=0;
					$Consulta = $CONEXION -> query("SELECT * FROM productostalla");
					$numeroTallas  = $Consulta->num_rows;
					while ($row_Consulta = $Consulta -> fetch_assoc()) {
						$tallaId = $row_Consulta['id'];

						$enproductos = $CONEXION -> query("SELECT id FROM productos WHERE tipotalla = '$tallaId'");
						$numeroCats = $enproductos->num_rows;

						$link='index.php?rand='.rand(1,90000).'&modulo='.$modulo.'&archivo=tallasdetalle&cat='.$tallaId;

						$borrarSubcat='<a href="javascript:eliminaTalla(id='.$tallaId.')" class="color-red" uk-icon="icon:trash"></a>';
						if ($numeroCats>0) {
							$borrarSubcat='<a class="uk-text-muted" uk-tooltip title="No puede eliminar<br>Existe en detalle de producto" uk-icon="icon:trash"></a>';
						}
						echo '
								<tr id="'.$row_Consulta['id'].'">
									<td class="uk-text-left">
										<input type="text" value="'.$row_Consulta['txt'].'" class="editarajax uk-input uk-form-small uk-form-blank" data-tabla="'.$modulocat.'" data-campo="txt" data-id="'.$row_Consulta['id'].'" tabindex="10" >
									</td>
									<td class="uk-text-nowrap">
										'.$borrarSubcat.' 
									</td>
								</tr>';
					}

			echo '
				</tbody>
			</table>
		</div>
	</div>
	';



// MODAL NUEVA CATEGORÍA
	echo '
	<div id="agregar" uk-modal="center: true" class="modal">
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" onsubmit="return checkForm(this);">

				<input type="hidden" name="nuevatalla" value="1">
				<input type="hidden" name="modulo" value="'.$modulo.'">
				<input type="hidden" name="archivo" value="'.$archivo.'">

				<label for="txt">Talla</label><br><br>
				<input type="text" name="txt" class="uk-input" required><br><br><br>

				<a class="uk-button uk-button-white uk-modal-close">Cerrar</a>
				<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
			</form>
		</div>
	</div>
	';


$scripts='
	// Eliminar
		function eliminaTalla () { 
			var statusConfirm = confirm("Realmente desea eliminar esta Talla?"); 
			if (statusConfirm == true) { 
				window.location = ("index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo='.$archivo.'&eliminarTalla&id='.$tallaId.'&id="+id);
			} 
		};
';

