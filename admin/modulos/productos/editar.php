<?php
	$consulta = $CONEXION -> query("SELECT * FROM $modulo WHERE id = $id");
	$row_catalogo = $consulta -> fetch_assoc();
	$cat=$row_catalogo['categoria'];
	$marca=$row_catalogo['marca'];

	$CATEGORY = $CONEXION -> query("SELECT * FROM $modulocat WHERE id = $cat");
	$row_CATEGORY = $CATEGORY -> fetch_assoc();
	$catNAME=$row_CATEGORY['txt'];
	$catParentID=$row_CATEGORY['parent'];

	$CATEGORY = $CONEXION -> query("SELECT * FROM $modulocat WHERE id = $catParentID");
	$row_CATEGORY = $CATEGORY -> fetch_assoc();
	$catParent=$row_CATEGORY['txt'];

// BREADCRUMB
	echo '
	<div class="uk-width-1-1 margin-v-20">
		<ul class="uk-breadcrumb uk-text-center">
			<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=categorias">Categorías</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=catdetalle&cat='.$catParentID.'">'.$catParent.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=items&cat='.$cat.'">'.$catNAME.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=detalle&id='.$id.'">'.$row_catalogo['sku'].'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=editar&id='.$id.'" class="color-red">Editar</a></li>
		</ul>
	</div>';
	

// inicio
	echo '
	<div class="uk-width-1-1 margin-top-20 uk-form">
		<div class="uk-container">
			<form action="index.php" method="post" enctype="multipart/form-data" name="datos" onsubmit="return checkForm(this);">
				<input type="hidden" name="editar" value="1">
				<input type="hidden" name="modulo" value="'.$modulo.'">
				<input type="hidden" name="archivo" value="detalle">
				<input type="hidden" name="id" value="'.$id.'">
				<div uk-grid class="uk-grid-small uk-child-width-1-3@s">
					<div>
						<label for="sku">SKU</label>
						<input type="text" class="uk-input" name="sku" value="'.$row_catalogo['sku'].'" autofocus required>
					</div>
					<div>
						<label for="titulo">Modelo</label>
						<input type="text" class="uk-input" name="titulo" value="'.$row_catalogo['titulo'].'" required>
					</div>
					

					<div>
						<label for="categoria">Categoría y subCategoría</label>
						<div>
							<select name="categoria" data-placeholder="Seleccione una" class="chosen-select uk-select" required>
								<option value=""></option>';
									$CONSULTA = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0 ORDER BY txt");
									while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
										$parentId=$row_CONSULTA['id'];
										$parentTxt=$row_CONSULTA['txt'];
										echo '
											<optgroup label="'.$parentTxt.'">';
										$CONSULTA1 = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = $parentId ORDER BY txt");
										while ($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
											$estatus=(isset($cat) AND $cat==$row_CONSULTA1['id'])?'selected':'';
											echo '
											<option value="'.$row_CONSULTA1['id'].'" '.$estatus.'>'.$row_CONSULTA1['txt'].'</option>';
										}
										echo '
											</optgroup>';
									}
									echo '
							</select>
						</div>
					</div>
					<div>
						<label for="tipotalla">Tipo de talla</label>
						<div>
							<select name="tipotalla" data-placeholder="Seleccione una" class="chosen-select uk-select" required>
								<option value=""></option>';
								$CONSULTA1 = $CONEXION -> query("SELECT * FROM productostalla ORDER BY txt");
								while ($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
									$estatus=($row_catalogo['tipotalla']==$row_CONSULTA1['id'])?'selected':'';
									echo '
									<option value="'.$row_CONSULTA1['id'].'" '.$estatus.'>'.$row_CONSULTA1['txt'].'</option>';
								}
								echo '
							</select>
						</div>
					</div>
					<div>
						<label for="descuento">Descuento</label>
						<input type="text" class="uk-input input-number descuento" name="descuento" value="'.$row_catalogo['descuento'].'" required>
					</div>
				</div>
				<div uk-grid class="uk-grid-small uk-child-width-1-4@s">					
					<div>
						<label for="precio">Precio publico</label>
						<input type="text" class="uk-input input-number" name="precio"  value="'.$row_catalogo['precio'].'" min="0" required>
					</div>
					<div>
						<label for="precio1">Precio distribuidor</label>
						<input type="text" class="uk-input input-number" name="precio1"  value="'.$row_catalogo['precio1'].'" min="0" required>
					</div>
					<div>
						<label for="precio2">Precio mayoreo</label>
						<input type="text" class="uk-input input-number" name="precio2"  value="'.$row_catalogo['precio2'].'" min="0" required>
					</div>
					<div>
						<label for="precio3">Precio concesionario</label>
						<input type="text" class="uk-input input-number" name="precio3"  value="'.$row_catalogo['precio3'].'" min="0" required>
					</div>
				</div>
<style type="text/css">
.color{font-weight: bold;}
</style>
				<div class="uk-container">
					<div><br><br>
						<label for="precio3"> Selecciona los colores para este Producto </label>
					</div>
					<div class="uk-width-1-1">
						<div uk-grid class="uk-grid-small uk-flex uk-flex-center uk-margin-remove uk-padding-small">
						';
						$idProd = $row_catalogo['id'];
						$CONSULTA = $CONEXION -> query("SELECT * FROM productoscolor ORDER BY txt");
						while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
							$ids=$row_CONSULTA['id'];
							$chequed = "";
							$font = "";
							$consultaRel = $CONEXION -> query("SELECT * FROM productos_has_color WHERE id_producto = $idProd AND id_color = $ids");
							if($consultaRel -> num_rows > 0){
								$chequed = "checked";
								$font = "color";
							}

								echo '
								<div class="uk-width-1-4@s uk-margin-remove uk-padding-small">
									<div class="uk-width-1-1 uk-card uk-card-default uk-card-body uk-grid-small uk-margin-remove uk-padding-small" uk-grid style="border:solid #eee 1px">
										<div class="uk-width-auto">
											<div style="border: solid 1px;
										    border-radius: 100%;
										    width: 20px;
										    height: 20px;
											background:'.$row_CONSULTA["txt"].';">  </div>
										</div>
										<div class="uk-width-expand '.$font.'">
										'.$row_CONSULTA["name"].'
										</div>
										<div class="uk-width-1-5">
											<input class="uk-checkbox" type="checkbox" 
											name="colores[]"
											value="'.$row_CONSULTA['id'].'" '.$chequed.' />
										</div>
									</div>
								</div>
								';
								
						}

						echo'
						</div>
					</div>
				</div>

				<div uk-grid class="uk-grid-small uk-child-width-1-2@s">
					
					
				</div>

				<div class="uk-margin">
					<label for="txt">Descripción</label>
					<textarea class="editor" name="txt" id="txt">'.$row_catalogo['txt'].'</textarea>
				</div>

				<div class="uk-margin">
					<label for="title">Título google</label>
					<input type="text" class="uk-input" name="title" value="'.$row_catalogo['title'].'">
				</div>
				<div class="uk-margin">
					<label for="metadescription">Descripción google</label>
					<textarea class="uk-textarea" name="metadescription">'.$row_catalogo['metadescription'].'</textarea>
				</div>
				<div class="uk-margin uk-text-center">
					<a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=detalle&id='.$id.'" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
					<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
				</div>

			</form>
		</div>
	</div>
	';

