<?php
// BREADCRUMB
	if (isset($_GET['cat'])) {
		$CATEGORIAS = $CONEXION -> query("SELECT * FROM $modulocat WHERE id = $cat");
		$row_CATEGORIAS = $CATEGORIAS -> fetch_assoc();
		$catNAME=$row_CATEGORIAS['txt'];
		$parent=$row_CATEGORIAS['parent'];
		$CATPARENT = $CONEXION -> query("SELECT * FROM $modulocat WHERE id = $parent");
		$row_CATPARENT = $CATPARENT -> fetch_assoc();
		$parentName=$row_CATPARENT['txt'];
		echo '
		<div class="uk-width-1-1 margin-v-20 uk-text-left">
			<ul class="uk-breadcrumb uk-text-capitalize">
				<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'">Productos</a></li>
				<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=categorias">Categorías</a></li>
				<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=catdetalle&cat='.$parent.'">'.$parentName.'</a></li>
				<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=items&cat='.$cat.'">'.$catNAME.'</a></li>
				<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=nuevo&cat='.$cat.'" class="color-red">Nuevo</a></li>
			</ul>
		</div>';
	}else{
		echo '
		<div class="uk-width-1-1 margin-v-20 uk-text-left">
			<ul class="uk-breadcrumb uk-text-capitalize">
				<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'">Productos</a></li>
				<li><a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=nuevo" class="color-red">Nuevo</a></li>
			</ul>
		</div>';
	}



// DATOS
	echo '
	<div class="uk-width-1-1 margin-top-20 uk-form">
		<div class="uk-container">
			<form action="index.php" method="post" enctype="multipart/form-data" name="datos" onsubmit="return checkForm(this);">
				<input type="hidden" name="nuevo" value="1">
				<input type="hidden" name="modulo" value="'.$modulo.'">
				<input type="hidden" name="archivo" value="'.$archivo.'">
				<div uk-grid class="uk-grid-small uk-child-width-1-3@s">
					<div>
						<label for="sku">SKU</label>
						<input type="text" class="uk-input" name="sku" autofocus required>
					</div>
					<div>
						<label for="titulo">Modelo</label>
						<input type="text" class="uk-input" name="titulo" required>
					</div>
					
					<div>
						<label for="categoria">Categoría y subcategoría</label>
						<div>
							<select name="categoria" data-placeholder="Seleccione una" class="chosen-select uk-select" required>
								<option value=""></option>';
									$CONSULTA = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0 ORDER BY txt");
									$numCats=$CONSULTA->num_rows;
									if ($numCats==0) {
										// Si no hay categorías, entonces lo mandamos a que haga una
										$scripts='window.location = ("index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=categorias");';
									}
									while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
										$parentId=$row_CONSULTA['id'];
										$parentTxt=$row_CONSULTA['txt'];
										echo '
											<optgroup label="'.$parentTxt.'">';
										$CONSULTA1 = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = $parentId ORDER BY txt");
										$numCats=$CONSULTA1->num_rows;
										if ($numCats==0) {
											// Si no hay subcategorías, entonces lo mandamos a que haga una
											// $scripts='window.location = ("index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=categorias");';
										}
										while ($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
											if (isset($cat) AND $cat==$row_CONSULTA1['id']) {
												$estatus='selected';
											}else{
												$estatus='';
											}
											echo '
											<option value="'.$row_CONSULTA1['id'].'" '.$estatus.'>'.$row_CONSULTA1['txt'].'</option>';
										}
										echo '
											</optgroup>';
									}
									echo '
							</select>
						</div>
					</div>

					<div>
						<label for="tipotalla">Talla</label>
						<div>
							<select name="tipotalla" data-placeholder="Seleccione una" class="chosen-select uk-select" required>
								<option value=""></option>';
								$CONSULTA1 = $CONEXION -> query("SELECT * FROM productostalla ORDER BY txt");
								$numTallas=$CONSULTA1->num_rows;
								if ($numTallas==0) {
									// Si no hay subcategorías, entonces lo mandamos a que haga una
									$scripts='window.location = ("index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=tallas");';
								}
								while ($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
									echo '
									<option value="'.$row_CONSULTA1['id'].'" '.$estatus.'>'.$row_CONSULTA1['txt'].'</option>';
								}
								echo '
							</select>
						</div>
					</div>
					<!--<div>
						<label for="colores">Colores</label>
						<div>
							<select name="tipotalla" data-placeholder="Seleccione una" class="chosen-select uk-select" required>
								<option value=""></option>';
								$CONSULTA1 = $CONEXION -> query("SELECT * FROM productoscolor ORDER BY txt");
								$numColores=$CONSULTA1->num_rows;
								if ($numColores==0) {
									// Si no hay subcategorías, entonces lo mandamos a que haga una
									$scripts='window.location = ("index.php?rand='.rand(1,1000).'&modulo='.$modulo.'&archivo=cfgcolores");';
								}
								while ($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
									echo '
									<option value="'.$row_CONSULTA1['id'].'" '.$estatus.' style="border:solid #fff 2px;background-color:'.$row_CONSULTA1['txt'].'">
										<div style="border:solid #fff 2px;background-color:'.$row_CONSULTA1['txt'].'">'.$row_CONSULTA1['name'].'
										</div>
									</option>';
								}
								echo '
							</select>
						</div>
					</div>-->
					<div>
						<label for="descuento">Descuento</label>
						<input type="text" class="uk-input input-number descuento" name="descuento" value="0" min="0" required>
					</div>
				</div>

				<div uk-grid class="uk-grid-small uk-child-width-1-4@s">					
					<div>
						<label for="precio">Precio publico</label>
						<input type="text" class="uk-input input-number" name="precio" min="0" required>
					</div>
					<div>
						<label for="precio1">Precio distribuidor</label>
						<input type="text" class="uk-input input-number" name="precio1" min="0" required>
					</div>
					<div>
						<label for="precio2">Precio mayoreo</label>
						<input type="text" class="uk-input input-number" name="precio2" min="0" required>
					</div>
					<div>
						<label for="precio3">Precio concesionario</label>
						<input type="text" class="uk-input input-number" name="precio3" min="0" required>
					</div>
				</div>
				<div class="uk-container">
					<div><br><br>
						<label for="precio3"> Selecciona los colores para este Producto </label>
					</div>
					<div class="uk-width-1-1">
						<div uk-grid class="uk-grid-small uk-flex uk-flex-center uk-margin-remove uk-padding-small">
						';
						$CONSULTA = $CONEXION -> query("SELECT * FROM productoscolor ORDER BY txt");
						while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
							$ids=$row_CONSULTA['id'];
							$chequed = "";
							
							echo '
							<div class="uk-width-1-4@s uk-margin-remove uk-padding-small" uk-grid>
								<div class="uk-width-1-1 uk-card uk-card-default uk-card-body uk-grid-small uk-margin-remove uk-padding-small" uk-grid style="border:solid #eee 1px">
									<div class="uk-width-auto">
										<div style="border: solid 1px;
									    border-radius: 100%;
									    width: 20px;
									    height: 20px;
										background:'.$row_CONSULTA["txt"].';">  </div>
									</div>
									<div class="uk-width-expand">
									'.$row_CONSULTA["name"].'
									</div>
									<div class="uk-width-1-5">
										<input class="uk-checkbox" type="checkbox" 
										name="colores[]"
										value="'.$row_CONSULTA['id'].'" '.$chequed.' />
									</div>
								</div>
							</div>
							';
							}

						echo'
						</div>
					</div>
				</div>
				<div class="uk-margin">
					<label for="txt">Descripción</label>
					<textarea class="editor" name="txt" id="txt"></textarea>
				</div>

				<div class="uk-margin">
					<label for="title">Título google</label>
					<input type="text" class="uk-input" name="title">
				</div>
				<div class="uk-margin">
					<label for="metadescription">Descripción google</label>
					<textarea class="uk-textarea" name="metadescription"></textarea>
				</div>
				<div class="uk-margin uk-text-center">
					<a href="index.php?rand='.rand(1,1000).'&modulo='.$modulo.'" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>
					<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
				</div>

			</form>
		</div>
	</div>
	';