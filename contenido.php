<?php 

$frame=(isset($_REQUEST['frame']))?$_REQUEST['frame']:'inicio';

$estatusInicio		='white';
$estatusSoluciones  ='white';
$estatusNosotros	='white';
$estatusPortafolio	='white';


switch ($frame) {
	case 'inicio':
		$estatusInicio='primary';
		break;
	case 'nosotros':
		$estatusNosotros='primary';
		break;
	case 'soluciones':
		$estatusSoluciones='primary';
		break;
	case 'portafolio':
		$estatusPortafolio='primary';
		break;
	default:
		$estatusInicio='primary';
		break;
}
	echo'
	<div class="uk-width-1-3@s margen-top-20 uk-text-left">
		<ul class="uk-breadcrumb">
			<li><a href="index.php?seccion='.$seccion.'" class="color-red uk-text-capitalize">Traducción</a></li>
		</ul>
	</div>

	
	<div class="uk-width-1-1 margen-top-20">
		<ul uk-switcher class="uk-subnav uk-subnav-pill uk-flex uk-flex-right">';
		$CONSULTA = $CONEXION -> query("SELECT DISTINCT seccion FROM $seccion ORDER BY seccion");
		while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
		echo '
			<li>
				<a href="#">'.$row_CONSULTA['seccion'].'</a>
			</li>';
		}
		echo '
		</ul>
	
		<ul class="uk-switcher uk-margin">';

		$CONSULTA = $CONEXION -> query("SELECT DISTINCT seccion FROM $seccion ORDER BY seccion");
		while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
			$columna = $row_CONSULTA['seccion'];
			$imagen = '';
			echo '
		    <li>';
			$CONSULTA3 = $CONEXION -> query("SELECT * FROM traduccionpic WHERE seccion = '$columna'");
			while($row_CONSULTA3 = $CONSULTA3 -> fetch_assoc()){
				$color = $row_CONSULTA3['color'];
				$imagen = $row_CONSULTA3['imagen'];
				$imagenId = $row_CONSULTA3['id'];

				if ($columna=='inicio') {
					echo'
					<div uk-grid>
						<div class="uk-width-1-3@s" style="margin-top:10px;>
							<label for="color">Color Bloque 1 ("#e6007e" o "rgb(25,169,200)")</label>
							<input id="color-input" type="text" class="uk-input selector" data-tabla="traduccionpic" data-campo="color" data-id="'.$row_CONSULTA3['id'].'" name="color" value="'.$row_CONSULTA3['color'].'" style="background-color: '.$row_CONSULTA3['color'].'">
						</div>
						<div class="uk-width-1-3@s" style="margin-top:10px;>
							<label for="color">Color Bloque 2 ("#1e87f0" o "rgb(25,169,200)")</label>
							<input id="color-input2" type="text" class="uk-input selector" data-tabla="traduccionpic" data-campo="color2" data-id="'.$row_CONSULTA3['id'].'" name="color2" value="'.$row_CONSULTA3['color2'].'" style="background-color: '.$row_CONSULTA3['color2'].'">
						</div>
						<div class="uk-width-1-3@s" style="margin-top:10px;>
							<label for="color">Color Bloque 3 ("#ffff00" o "rgb(25,169,200)")</label>
							<input id="color-input3" type="text" class="uk-input selector" data-tabla="traduccionpic" data-campo="color3" data-id="'.$row_CONSULTA3['id'].'" name="color3" value="'.$row_CONSULTA3['color3'].'" style="background-color: '.$row_CONSULTA3['color3'].'">
						</div>
					</div>
					';
				}else{
				echo '
					<div class="uk-width-1-3" style="margin-top:10px;>
						<label for="color">Color Menu (ejemplo de sintaxis: "#1e87f0" o "rgb(25,169,200)")</label>
						<input id="color-input" type="text" class="uk-input selector" data-tabla="traduccionpic" data-campo="color" data-id="'.$row_CONSULTA3['id'].'" name="color" value="'.$row_CONSULTA3['color'].'" style="background-color: '.$row_CONSULTA3['color'].'">
					</div>
					<div uk-grid>
						<div class="uk-width-1-2" style="margin-top:10px;>
							<label for="tituloes">Titulo menu Español</label>
							<input type="text" class="uk-input selector" data-tabla="traduccionpic" data-campo="tituloes" data-id="'.$row_CONSULTA3['id'].'" name="tituloes" value="'.$row_CONSULTA3['tituloes'].'" ">
						</div>
						<div class="uk-width-1-2" style="margin-top:10px;>
							<label for="tituloen">Titulo menu Ingles</label>
							<input type="text" class="uk-input selector" data-tabla="traduccionpic" data-campo="tituloen" data-id="'.$row_CONSULTA3['id'].'" name="tituloen" value="'.$row_CONSULTA3['tituloen'].'" ">
						</div>';
							if ($columna=='nosotros') {
								echo '
							<div class="uk-width-1-2" style="margin-top:10px;>
								<label for="url">Link del video (link de youtube)</label>
								<input type="text" class="uk-input selector" data-tabla="traduccionpic" data-campo="url" data-id="'.$row_CONSULTA3['id'].'" name="url" value="'.$row_CONSULTA3['url'].'" ">
							</div>';
							}

					echo '</div>
					<div class="uk-width-1-3 uk-text-center margen-v-20">
					</div>';
				}
			}

				echo '
		    	<div class="uk-width-1-1 margen-v-50">
					<table class="uk-table uk-table-small uk-table-striped uk-table-hover">
						<thead>
							<tr class="uk-text-muted">';
							if ($debug==1) {
								echo '
								<td>Variable</td>';
							}
							echo '
								<td>Español</td>
								<td>Inglés</td>
							</tr>
						</thead>
						<tbody>';

						$CONSULTA1 = $CONEXION -> query("SELECT * FROM $seccion WHERE seccion = '$columna' ORDER BY id");
						while ($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
							$thisId=$row_CONSULTA1['id'];

							echo '
							<tr>';
							if ($debug==1) {
								echo '
								<td>
									'.$row_CONSULTA1['variable'].'
								</td>';
							}
							echo '
								<td>
									<input type="text" class="datos uk-input uk-width-1-1 uk-form-small" data-lang="es" data-id="'.$row_CONSULTA1['id'].'" value="'.$row_CONSULTA1['es'].'">
								</td>
								<td>
									<input type="text" class="datos uk-input uk-width-1-1 uk-form-small" data-lang="en" data-id="'.$row_CONSULTA1['id'].'" value="'.$row_CONSULTA1['en'].'">
								</td>
							</tr>';
						}

				echo '
						</tbody>
					</table>
				</div>';
				if ($columna != 'inicio') {

					echo '				
					<div class="uk-width-1-1 margen-v-50">
						<hr class="uk-divider-icon">
					</div>

					<!-- Fotografía fondo de seccion -->
					<div class="uk-width-1-1">
						<h1 class="uk-text-center">Imagen para banner</h1>
					</div>
					<div uk-grid class="uk-width-1-1">
						<div class="uk-width-1-2 padding-v-50">
							<div class="margen-bottom-50 uk-text-muted">
								Dimensiones recomendadas: 
								1500 px de ancho 
								400 px de alto
							</div>
							<div id="fileuploadermain'.$columna.'">
								Cargar
							</div>
						</div>

						<div class="uk-width-1-2 uk-text-center margen-v-20">';

					$pic='../img/contenido/'.$seccion.'/'.$imagen;
					if(strlen($imagen)>0 AND file_exists($pic)){
						echo '
							<div class="uk-panel uk-text-center">
								<a href="'.$pic.'" target="_blank">
									<img src="'.$pic.'" class=" uk-border-rounded margen-top-20">
								</a><br><br>
								<button class="uk-button uk-button-danger borrarpic" data-id="'.$imagenId.'"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
							</div>';
					}elseif(strlen($imagen)>0 AND strpos($imagen, 'ttp')>0){
						echo '
							<div class="uk-panel uk-text-center">
								<div class="uk-width-1-1">
									<a href="'.$imagen.'" target="_blank">
										<img src="'.$imagen.'" class=" uk-border-rounded margen-top-20">
									</a>
								</div>
								<div class="uk-width-1-1">
									Link a la imagen:
								</div>
								<div class="uk-width-1-1">
									<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$imagenId.'" value="'.$imagen.'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
								</div>
							</div>';
					}else{
						echo '
							<div class="uk-panel uk-text-center">
								<div class="uk-width-1-1">
									<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
										<br><br>
									</p>
								</div>
							</div>';
					}
					echo '
						</div>
					</div>';

					//// FOTOS SLIDER NOSOTROS /////////////

					if ($columna == 'nosotros') {

						echo '
						<div class="uk-width-1-1 margen-v-50">
							<hr class="uk-divider-icon">
						</div>
						<div class="uk-width-1-1">
							<h1 class="uk-text-center">Imagenes para Carousel</h1>
						</div>
						<div class="uk-width-1-1">
							<div id="fileuploader">
								Cargar
							</div>
						</div>
						<div class="uk-width-1-1 uk-text-center">
							<div uk-grid class="uk-grid-small uk-grid-match sortable" data-tabla="snaf">';

						$consulta1 = $CONEXION -> query("SELECT * FROM lifestyle ORDER BY orden,id");
						while ($row_Consulta1 = $consulta1 -> fetch_assoc()) {

							$prodID=$row_Consulta1['id'];
							$estatusIcon=($row_Consulta1['estatus']==1)?'on uk-text-primary ':'off uk-text-muted';

							$pic='../img/contenido/lifestyle/'.$prodID.'.jpg';
							if(file_exists($pic)){
								echo '
								<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$prodID.'">
									<div class="uk-card uk-card-body uk-text-center" style="background: #ddd;">
										<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="snaf" data-campo="estatus" data-id="'.$prodID.'" data-valor="'.$row_Consulta1['estatus'].'"></i>
										<a href="'.$pic.'" class="uk-icon-button uk-button-default" target="_blank" uk-icon="icon:image"></a> &nbsp;
										<a href="javascript:eliminaPic(picID='.$prodID.')" class="uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></a>
										<br>
										<div style="padding-bottom: 50px;">
											<img src="'.$pic.'" class=" uk-border-rounded margen-top-20"><br>
										</div>
									</div>
								</div>';
							}else{
								echo '
								<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$prodID.'">
									<div class="uk-card uk-card-default uk-card-body uk-text-center">
										<a href="javascript:eliminaPic(picID='.$prodID.')" class="uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></a>
										<br>
										Imagen rota<br>
										<i uk-icon="icon:ban;ratio:2;"></i>
									</div>
								</div>';
							}
						}

						echo '	
							</div>
						</div>';


					}
					//// /FOTOS SLIDER NOSOTROS /////////////



				
			echo '
		    </li>';
			}
		}
		echo '
		</ul>
	</div>';

	$scripts='
		$(".datos").focusout(function() {
			var id = $(this).data("id");
			var lang = $(this).data("lang");
			var valor = $(this).val();
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					editavalor:1,
					valor: valor,
					lang: lang,
					id: id
				}
			})
			.done(function( msg ) {
				UIkit.notification.closeAll();
				UIkit.notification(msg);
			});
		});

		// Borrar foto 
		$(".borrarpic").click(function() {
			var id = $(this).attr("data-id");
			var statusConfirm = confirm("Realmente desea borrar esto?"); 
			if (statusConfirm == true) { 
				window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&borrarpic&id="+id);
			} 
		});

		$(document).ready(function() {
		     $("#color-input").change(function(){
		     	var color = $(this).val();
		     	console.log(color);
				$(this).css("background-color",color);
		     });
		     $("#color-input2").change(function(){
		     	var color = $(this).val();
		     	console.log(color);
				$(this).css("background-color",color);
		     });
		     $("#color-input3").change(function(){
		     	var color = $(this).val();
		     	console.log(color);
				$(this).css("background-color",color);
		     });
		});


		$(document).ready(function() {
			$("#fileuploader").uploadFile({
				url:"../library/upload-file/php/upload.php",
				fileName:"myfile",
				maxFileCount:1,
				showDelete: \'false\',
				allowedTypes: "jpg",
				maxFileSize: 6291456,
				showFileCounter: false,
				showPreview:true,
				returnType:\'json\',
				onSuccess:function(data){ 
					window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&position=gallery&imagen1=\'+data);
				}
			});
		});

		function eliminaPic () { 
			var statusConfirm = confirm("Realmente desea eliminar esta foto?"); 
			if (statusConfirm == true) { 
				window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&borrarPic&id="+picID);
			} 
		};


		';

	$CONSULTA = $CONEXION -> query("SELECT * FROM traduccionpic");
	while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
		$columna = $row_CONSULTA['seccion'];
		$scripts.= '
		$(document).ready(function() {
			$("#fileuploadermain'.$columna.'").uploadFile({
				url:"../library/upload-file/php/upload.php",
				fileName:"myfile",
				maxFileCount:1,
				showDelete: \'false\',
				allowedTypes: "jpeg,jpg",
				maxFileSize: 6291456,
				showFileCounter: false,
				showPreview:false,
				returnType:\'json\',
				onSuccess:function(data){ 
					window.location = (\'index.php?seccion='.$seccion.'&id='.$row_CONSULTA['id'].'&imagen=\'+data);
				}
			});
		});
		';
	}

