-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-09-2020 a las 05:00:27
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ecomme`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` int(10) NOT NULL,
  `orden` int(2) NOT NULL DEFAULT 0,
  `titulo` varchar(300) DEFAULT NULL,
  `txt` text DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogpic`
--

CREATE TABLE `blogpic` (
  `id` int(10) NOT NULL,
  `item` int(10) NOT NULL,
  `titulo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(2) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendario`
--

CREATE TABLE `calendario` (
  `id` int(10) NOT NULL,
  `fecha` date DEFAULT NULL,
  `hora` int(2) DEFAULT NULL,
  `txt` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `folder` int(1) NOT NULL DEFAULT 1,
  `mapa` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `link` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt1` text COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendariopic`
--

CREATE TABLE `calendariopic` (
  `id` int(10) NOT NULL,
  `producto` int(10) NOT NULL,
  `titulo` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `title` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `txt` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carousel`
--

CREATE TABLE `carousel` (
  `id` int(10) NOT NULL,
  `orden` int(2) DEFAULT NULL,
  `titulo` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `txt` text COLLATE latin1_general_ci DEFAULT NULL,
  `url` text COLLATE latin1_general_ci DEFAULT NULL,
  `video` varchar(100) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Volcado de datos para la tabla `carousel`
--

INSERT INTO `carousel` (`id`, `orden`, `titulo`, `txt`, `url`, `video`) VALUES
(16, 99, NULL, NULL, NULL, NULL),
(19, 99, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carousel2`
--

CREATE TABLE `carousel2` (
  `id` int(10) NOT NULL,
  `orden` int(2) DEFAULT NULL,
  `titulo` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `txt` text COLLATE latin1_general_ci DEFAULT NULL,
  `url` text COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogos`
--

CREATE TABLE `catalogos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogospic`
--

CREATE TABLE `catalogospic` (
  `id` int(11) NOT NULL,
  `producto` int(11) DEFAULT NULL,
  `alt` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(2) NOT NULL,
  `title` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `description` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `prodspag` int(5) DEFAULT NULL,
  `sliderhmin` int(5) DEFAULT 0,
  `sliderhmax` int(5) DEFAULT 1000,
  `sliderproporcion` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `slideranim` int(1) DEFAULT NULL,
  `slidertextos` int(1) DEFAULT NULL,
  `paypalemail` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `destinatario1` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `destinatario2` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `remitente` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `remitentepass` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `remitentehost` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `remitenteport` varchar(5) COLLATE latin1_spanish_ci DEFAULT NULL,
  `remitenteseguridad` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono1` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `instagram` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `youtube` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `envio` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `envioglobal` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `iva` int(2) DEFAULT NULL,
  `incremento` int(2) DEFAULT NULL,
  `bank` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyct1` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyct2` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyct3` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyct4` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyc1` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyc2` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyc3` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyc4` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `pdf1` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagen1` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagen2` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagen3` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagen4` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `color1` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `color2` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `about1` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `about2` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `about3` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `about_titulo1` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `about_titulo2` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `about_titulo3` text COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `title`, `description`, `prodspag`, `sliderhmin`, `sliderhmax`, `sliderproporcion`, `slideranim`, `slidertextos`, `paypalemail`, `destinatario1`, `destinatario2`, `remitente`, `remitentepass`, `remitentehost`, `remitenteport`, `remitenteseguridad`, `telefono`, `telefono1`, `facebook`, `instagram`, `youtube`, `envio`, `envioglobal`, `iva`, `incremento`, `bank`, `tyct1`, `tyct2`, `tyct3`, `tyct4`, `tyc1`, `tyc2`, `tyc3`, `tyc4`, `pdf1`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `color1`, `color2`, `about1`, `about2`, `about3`, `about_titulo1`, `about_titulo2`, `about_titulo3`) VALUES
(1, 'ECOME', 'Productos ecologicos \ntermos plegables\ntuppers plegables\n\n\nAqu&iacute; colocamos el texto que se utilizara para la descripci&oacute;n de sitio, hablamos sobre los productos; son las palabras claves que se utilizan para google', 8, 300, 700, '1:1', 0, 0, 'business@wozial.com', '', '', 'contacto@eshot.mx', 'LeGuaGua@ElPerrito', 'mail.eshot.mx', '465', 'SSL', '', '', 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://pinterest.com.mx/', '0', '100', 0, 0, '', 'NUESTRA MISI&Oacute;N', 'TERMINOS Y CONDICIONES', 'POPL&Iacute;TICAS DE ENVIO', 'Términos y condiciones', '<p>mision ECOME</p>', '<p>terminos ECOME</p>', '<p>politicas ECOME</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id nulla ac libero viverra laoreet. Duis varius scelerisque nunc at feugiat. Sed viverra est non fringilla pellentesque. Sed dictum suscipit tristique. In ultricies neque vel aliquam pharetra. Aliquam magna dolor, accumsan a mi id, commodo consequat purus. Nullam lobortis erat a tempor blandit.</p>\r\n<p>In nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst.</p>\r\n<p>Aenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet.</p>', NULL, '590391764.jpg', '838399456.png', '808594938.jpg', '114183711.jpg', 'rgba(110,190,71, .5)', 'rgba(70, 135, 38, 0.7)', '<p><span style=\"font-family: Lato, sans-serif; font-size: 16.2px;\">NOSOTROS<br />Somos una empresa mexica dedicada a distribuir productos para reducir el uso de plastico sustituyendo por productos mas amigables con el ambiente y de facil transportaci&oacute;n. Resolvemos problemas de la vida diaria para que le podamos decir adi&oacute;s a los productos de un solo uso.<br /></span></p>', '<p>MISION<br />Ayudar a nuestros clientes a tener una fuente de ingreso extra sin necesidad de invertir en un muestrario&nbsp;</p>', '<p>Habremos logrado satisfacer el gusto delicado del mercado nacional y del extranjero, sinti&eacute;ndonos orgullosos de que el producto hecho en M&eacute;xico sea del agrado de pa&iacute;ses como Canad&aacute;, Estados Unidos y algunos pa&iacute;ses de Europa.</p>\r\n<p>Manejando la m&aacute;s alta calidad en cada uno de nuestros productos hechos con pieles seleccionadas especialmente para nuestos clientes.</p>', 'SOBRE NOSOTROS', 'NUESTRA MISI&Oacute;N', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT 99,
  `estatus` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq`
--

CREATE TABLE `faq` (
  `id` int(5) NOT NULL,
  `orden` int(2) NOT NULL,
  `pregunta` text NOT NULL,
  `respuesta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoritos`
--

CREATE TABLE `favoritos` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `producto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias`
--

CREATE TABLE `galerias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeriaspic`
--

CREATE TABLE `galeriaspic` (
  `id` int(11) NOT NULL,
  `producto` int(11) DEFAULT NULL,
  `alt` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ipn`
--

CREATE TABLE `ipn` (
  `id` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `email` varchar(50) DEFAULT NULL,
  `txn_id` varchar(50) DEFAULT NULL,
  `pedido` int(10) DEFAULT NULL,
  `ipn` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(10) NOT NULL,
  `idmd5` varchar(50) DEFAULT NULL,
  `uid` int(10) NOT NULL DEFAULT 0,
  `nombre` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `estatus` int(1) NOT NULL DEFAULT 0,
  `invisible` int(1) NOT NULL DEFAULT 0,
  `papelera` int(1) NOT NULL DEFAULT 0,
  `notify` int(1) NOT NULL DEFAULT 0,
  `guia` varchar(20) DEFAULT NULL,
  `linkguia` varchar(100) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `dom` int(11) NOT NULL DEFAULT 0,
  `factura` int(11) DEFAULT 0,
  `tabla` text DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL,
  `envio` decimal(15,2) DEFAULT NULL,
  `comprobante` varchar(50) DEFAULT NULL,
  `imagen` varchar(10) DEFAULT NULL,
  `ipn` varchar(50) DEFAULT NULL,
  `calle` varchar(100) DEFAULT NULL,
  `noexterior` varchar(50) DEFAULT NULL,
  `nointerior` varchar(50) DEFAULT NULL,
  `entrecalles` varchar(200) DEFAULT NULL,
  `pais` varchar(20) DEFAULT 'Mexico',
  `estado` varchar(50) DEFAULT NULL,
  `municipio` varchar(50) DEFAULT NULL,
  `colonia` varchar(50) DEFAULT NULL,
  `cp` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidosdetalle`
--

CREATE TABLE `pedidosdetalle` (
  `id` int(11) NOT NULL,
  `pedido` int(11) DEFAULT NULL,
  `producto` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `productotxt` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` decimal(15,2) DEFAULT NULL,
  `importe` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(10) NOT NULL,
  `categoria` int(2) DEFAULT NULL,
  `clasif` int(2) DEFAULT NULL,
  `tipotalla` int(11) DEFAULT NULL,
  `marca` int(2) DEFAULT NULL,
  `existencias` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `metadescription` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `precio` decimal(20,2) DEFAULT NULL,
  `precio2` decimal(20,2) DEFAULT 0.00,
  `precio3` decimal(20,2) DEFAULT 0.00,
  `precio1` decimal(20,0) NOT NULL DEFAULT 0,
  `descuento` int(2) NOT NULL DEFAULT 0,
  `titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sku` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `material` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `inicio` int(1) NOT NULL DEFAULT 0,
  `estatus` int(1) NOT NULL DEFAULT 1,
  `fecha` date DEFAULT NULL,
  `orden` int(2) DEFAULT 99,
  `forro` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `herraje` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `categoria`, `clasif`, `tipotalla`, `marca`, `existencias`, `title`, `metadescription`, `precio`, `precio2`, `precio3`, `precio1`, `descuento`, `titulo`, `sku`, `txt`, `material`, `imagen`, `inicio`, `estatus`, `fecha`, `orden`, `forro`, `herraje`) VALUES
(40, 29, NULL, 8, NULL, NULL, '', '', '550.00', '770.00', '880.00', '660', 1, 'uno Acero', '1', '<p>Suspendisse mattis sem ut lacus elementum, ac suscipit ex laoreet. Aliquam pharetra, sem et ullamcorper molestie, orci arcu commodo nibh, sit amet facilisis augue leo dignissim orci. Morbi suscipit tellus et augue tincidunt, ut fringilla quam porta.</p>\r\n<p>Nullam consequat pretium pulvinar.</p>\r\n<p>Phasellus tincidunt enim id purus placerat sodales. Proin posuere risus nec nulla porta, at mollis mauris gravida. Nullam libero odio, eleifend vel fermentum gravida, faucibus a nulla. Vivamus cursus, velit quis fringilla interdum, libero augue sagittis nisl, eget molestie ligula mauris sed ante. Pellentesque a vulputate arcu.</p>', NULL, NULL, 1, 1, '2020-08-28', 99, NULL, NULL),
(41, 29, NULL, 7, NULL, NULL, '', '', '400.00', '200.00', '100.00', '300', 4, 'dos Acero', 'dos', '<p>Suspendisse mattis sem ut lacus elementum, ac suscipit ex laoreet. Aliquam pharetra, sem et ullamcorper molestie, orci arcu commodo nibh, sit amet facilisis augue leo dignissim orci. Morbi suscipit tellus et augue tincidunt, ut fringilla quam porta. Nullam consequat pretium pulvinar. Phasellus tincidunt enim id purus placerat sodales. Proin posuere risus nec nulla porta, at mollis mauris gravida. Nullam libero odio, eleifend vel fermentum gravida, faucibus a nulla. Vivamus cursus, velit quis fringilla interdum, libero augue sagittis nisl, eget molestie ligula mauris sed ante. Pellentesque a vulputate arcu.</p>', NULL, NULL, 0, 1, '2020-08-28', 99, NULL, NULL),
(42, 31, NULL, 1, NULL, NULL, '', '', '1.00', '3.00', '4.00', '2', 1, 'a', 'a', '<p>aaa</p>', NULL, NULL, 0, 1, '2020-09-01', 99, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoscat`
--

CREATE TABLE `productoscat` (
  `id` int(11) NOT NULL,
  `parent` int(2) NOT NULL,
  `txt` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `descripcion` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagenhover` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `estatus` int(1) NOT NULL DEFAULT 0,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `productoscat`
--

INSERT INTO `productoscat` (`id`, `parent`, `txt`, `descripcion`, `imagen`, `imagenhover`, `estatus`, `orden`) VALUES
(23, 0, 'Silic&oacute;n', 'Por su composici&oacute;n qu&iacute;mica de silicio y ox&iacute;geno, es flexible y suave al tacto, no mancha, no se desgasta, no envejece, es resistente al uso que le den, no contamina, y puede adoptar formas y lucirse en colores, tiene una baja conductividad t&eacute;rmica, y una baja reactividad qu&iacute;mica, no es compatible con el crecimiento microbiol&oacute;gico y no es t&oacute;xica. Adem&aacute;s de resistir temperaturas extremas (&ndash;60 a 250 &deg;C)', NULL, NULL, 0, 99),
(24, 0, 'Bamb&uacute;', 'Es un material sostenible, porque es una de las plantas que crece más rápido del planeta, es biodegradable, tiene propiedades antibacterianas y de auto-limpieza.', NULL, NULL, 0, 99),
(25, 0, 'Cuidado personal', 'Nuestra linea de cuidado personal es libre de empaques plasticos, hecho con ingredientes naturales y libre de parabenos', NULL, NULL, 0, 99),
(26, 0, 'Termos', 'Hechos de materiales de larga duración y de fácil limpieza. ¡Lleva tu termo contigo! Es recomendable tomar 2 litros o más de agua al día, esto ayudará a regular tu temperatura corporal y el transporte adecuado de los nutrientes; además de liberar el estrés. ', NULL, NULL, 0, 99),
(27, 0, 'Textil', 'Hecho con materiales resistentes para que tengan una larga vida útil, es orgullosamente mexicano.', NULL, NULL, 0, 99),
(28, 0, 'Acero inoxidable', 'El acero inoxidable tiene una gran durabilidad, resistencia al calor y al frío. La facilidad de limpieza lo hace la primera opción en hospitales, cocinas e instrumentos alimenticios.', NULL, NULL, 0, 99),
(29, 28, 'Sub Acero uno', NULL, NULL, NULL, 0, 99),
(30, 23, 'Sub Silicion uno', NULL, NULL, NULL, 0, 99),
(31, 23, 'Sub Silicion dos', NULL, NULL, NULL, 0, 99),
(32, 24, 'Sub Bambu uno', NULL, NULL, NULL, 0, 99),
(33, 25, 'Sub Personal uno', NULL, NULL, NULL, 0, 99),
(34, 26, 'Sub Termos uno', NULL, NULL, NULL, 0, 99),
(35, 27, 'Sub Textil uno', NULL, NULL, NULL, 0, 99),
(36, 23, 'Sub Silicon Tres', NULL, NULL, NULL, 0, 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoscolor`
--

CREATE TABLE `productoscolor` (
  `id` int(11) NOT NULL,
  `txt` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `name` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `productoscolor`
--

INSERT INTO `productoscolor` (`id`, `txt`, `name`) VALUES
(10, '#2e06f4', 'azul'),
(11, '#1af50a', 'verde'),
(12, '#ab0ae6', 'morado'),
(13, '#ff9705', 'anaranjado'),
(14, '#f500cc', 'rosa'),
(15, '#f5e105', 'amarillo'),
(17, '#0f0000', 'negro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productosexistencias`
--

CREATE TABLE `productosexistencias` (
  `id` int(11) NOT NULL,
  `producto` int(11) DEFAULT NULL,
  `talla` int(11) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `existencias` int(11) DEFAULT NULL,
  `precio` decimal(10,2) NOT NULL DEFAULT 0.00,
  `estatus` int(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productosexistencias`
--

INSERT INTO `productosexistencias` (`id`, `producto`, `talla`, `color`, `existencias`, `precio`, `estatus`) VALUES
(1, 1, 1, 2, 0, '0.00', 0),
(2, 1, 1, 1, 0, '0.00', 1),
(3, 1, 2, 2, 0, '0.00', 0),
(4, 1, 2, 1, 0, '0.00', 0),
(5, 1, 3, 2, 0, '0.00', 0),
(6, 1, 3, 1, 0, '0.00', 0),
(7, 20, 1, 1, 0, '0.00', 0),
(8, 20, 2, 1, 0, '0.00', 0),
(9, 20, 3, 1, 0, '0.00', 0),
(10, 20, 7, 1, 0, '0.00', 0),
(11, 20, 8, 1, 0, '0.00', 0),
(12, 20, 9, 1, 0, '0.00', 0),
(13, 20, 1, 17, 0, '0.00', 0),
(14, 20, 1, 11, 0, '0.00', 0),
(15, 20, 1, 10, 0, '0.00', 0),
(16, 20, 1, 12, 0, '0.00', 0),
(17, 20, 1, 14, 0, '0.00', 0),
(18, 20, 1, 15, 0, '0.00', 0),
(19, 20, 1, 13, 0, '0.00', 0),
(20, 20, 2, 17, 0, '0.00', 0),
(21, 20, 2, 11, 0, '0.00', 0),
(22, 20, 2, 10, 0, '0.00', 0),
(23, 20, 2, 12, 0, '0.00', 0),
(24, 20, 2, 14, 0, '0.00', 0),
(25, 20, 2, 15, 0, '0.00', 0),
(26, 20, 2, 13, 0, '0.00', 0),
(27, 20, 3, 17, 0, '0.00', 0),
(28, 20, 3, 11, 0, '0.00', 0),
(29, 20, 3, 10, 0, '0.00', 0),
(30, 20, 3, 12, 0, '0.00', 0),
(31, 20, 3, 14, 0, '0.00', 0),
(32, 20, 3, 15, 0, '0.00', 0),
(33, 20, 3, 13, 0, '0.00', 0),
(34, 20, 7, 17, 0, '0.00', 0),
(35, 20, 7, 11, 0, '0.00', 0),
(36, 20, 7, 10, 0, '0.00', 0),
(37, 20, 7, 12, 0, '0.00', 0),
(38, 20, 7, 14, 0, '0.00', 0),
(39, 20, 7, 15, 0, '0.00', 0),
(40, 20, 7, 13, 0, '0.00', 0),
(41, 20, 8, 17, 0, '0.00', 0),
(42, 20, 8, 11, 0, '0.00', 0),
(43, 20, 8, 10, 0, '0.00', 0),
(44, 20, 8, 12, 0, '0.00', 0),
(45, 20, 8, 14, 0, '0.00', 0),
(46, 20, 8, 15, 0, '0.00', 0),
(47, 20, 8, 13, 0, '0.00', 0),
(48, 20, 9, 17, 0, '0.00', 0),
(49, 20, 9, 11, 0, '0.00', 0),
(50, 20, 9, 10, 0, '0.00', 0),
(51, 20, 9, 12, 0, '0.00', 0),
(52, 20, 9, 14, 0, '0.00', 0),
(53, 20, 9, 15, 0, '0.00', 0),
(54, 20, 9, 13, 0, '0.00', 0),
(55, 21, 1, 17, 0, '0.00', 0),
(56, 21, 1, 11, 0, '0.00', 0),
(57, 21, 1, 10, 0, '0.00', 0),
(58, 21, 1, 12, 0, '0.00', 0),
(59, 21, 1, 14, 0, '0.00', 0),
(60, 21, 1, 15, 0, '0.00', 0),
(61, 21, 1, 13, 0, '0.00', 0),
(62, 21, 2, 17, 0, '0.00', 0),
(63, 21, 2, 11, 0, '0.00', 0),
(64, 21, 2, 10, 0, '0.00', 0),
(65, 21, 2, 12, 0, '0.00', 0),
(66, 21, 2, 14, 0, '0.00', 0),
(67, 21, 2, 15, 0, '0.00', 0),
(68, 21, 2, 13, 0, '0.00', 0),
(69, 21, 3, 17, 0, '0.00', 0),
(70, 21, 3, 11, 0, '0.00', 0),
(71, 21, 3, 10, 0, '0.00', 0),
(72, 21, 3, 12, 0, '0.00', 0),
(73, 21, 3, 14, 0, '0.00', 0),
(74, 21, 3, 15, 0, '0.00', 0),
(75, 21, 3, 13, 0, '0.00', 0),
(76, 21, 7, 17, 0, '0.00', 0),
(77, 21, 7, 11, 0, '0.00', 0),
(78, 21, 7, 10, 0, '0.00', 0),
(79, 21, 7, 12, 0, '0.00', 0),
(80, 21, 7, 14, 0, '0.00', 0),
(81, 21, 7, 15, 0, '0.00', 0),
(82, 21, 7, 13, 0, '0.00', 0),
(83, 21, 8, 17, 0, '0.00', 0),
(84, 21, 8, 11, 0, '0.00', 0),
(85, 21, 8, 10, 0, '0.00', 0),
(86, 21, 8, 12, 0, '0.00', 0),
(87, 21, 8, 14, 0, '0.00', 0),
(88, 21, 8, 15, 0, '0.00', 0),
(89, 21, 8, 13, 0, '0.00', 0),
(90, 21, 9, 17, 0, '0.00', 0),
(91, 21, 9, 11, 0, '0.00', 0),
(92, 21, 9, 10, 0, '0.00', 0),
(93, 21, 9, 12, 0, '0.00', 0),
(94, 21, 9, 14, 0, '0.00', 0),
(95, 21, 9, 15, 0, '0.00', 0),
(96, 21, 9, 13, 0, '0.00', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productosmarcas`
--

CREATE TABLE `productosmarcas` (
  `id` int(11) NOT NULL,
  `txt` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productospic`
--

CREATE TABLE `productospic` (
  `id` int(10) NOT NULL,
  `producto` int(10) NOT NULL,
  `titulo` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `title` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `txt` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `productospic`
--

INSERT INTO `productospic` (`id`, `producto`, `titulo`, `title`, `txt`, `orden`) VALUES
(1, 40, NULL, NULL, NULL, 99),
(2, 40, NULL, NULL, NULL, 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productostalla`
--

CREATE TABLE `productostalla` (
  `id` int(11) NOT NULL,
  `txt` text COLLATE latin1_spanish_ci DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `productostalla`
--

INSERT INTO `productostalla` (`id`, `txt`, `orden`) VALUES
(1, 'Chico', 99),
(2, 'Mediano', 99),
(3, 'Grande', 99),
(7, '500 ml', 99),
(8, '200ml', 99),
(9, '700 ml', 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productostallaclasif`
--

CREATE TABLE `productostallaclasif` (
  `id` int(11) NOT NULL,
  `txt` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productostallaclasif`
--

INSERT INTO `productostallaclasif` (`id`, `txt`, `orden`) VALUES
(2, 'Ropa', 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productostallarel`
--

CREATE TABLE `productostallarel` (
  `id` int(10) NOT NULL,
  `producto` int(2) DEFAULT NULL,
  `talla` int(3) DEFAULT NULL,
  `espalda` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `manga` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `largo` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `busto` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cintura` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_has_color`
--

CREATE TABLE `productos_has_color` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_color` int(11) DEFAULT NULL,
  `existencias` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `productos_has_color`
--

INSERT INTO `productos_has_color` (`id`, `id_producto`, `id_color`, `existencias`) VALUES
(58, 41, 17, 6),
(59, 41, 12, 10),
(69, 40, 10, 10),
(70, 40, 14, 8),
(71, 40, 13, 14),
(73, 42, 11, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE `sucursales` (
  `id` int(10) NOT NULL,
  `categoria` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `txtdetalle` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `url` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `estatus` int(1) NOT NULL DEFAULT 0,
  `fecha` date DEFAULT NULL,
  `orden` int(2) DEFAULT 99,
  `imagen` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lat` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lon` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`id`, `categoria`, `titulo`, `txt`, `txtdetalle`, `url`, `estatus`, `fecha`, `orden`, `imagen`, `lat`, `lon`) VALUES
(1, NULL, 'Matriz', '<p><strong>Av.</strong> Lapizl&aacute;zuli #2074<br /><strong>Col.</strong> Residencial Victoria<br /><strong>C.P.</strong> 44960</p>\r\n<p>Guadalajara, Jalisco; M&eacute;xico</p>', NULL, 'https://www.google.com/maps/place/Wozial+Marketing+Lovers/@20.6421948,-103.39869,17z/data=!3m1!4b1!4m5!3m4!1s0x8428ae0ed241a9bb:0xbb4c3906c38265fd!8m2!3d20.6421898!4d-103.3965013', 0, '2020-01-23', 99, NULL, '20.642174230499336', '-103.39650959025418');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testimonios`
--

CREATE TABLE `testimonios` (
  `id` int(10) NOT NULL,
  `titulo` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `txt` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `face` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `twit` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `insta` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `linkedin` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(2) DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `testimonios`
--

INSERT INTO `testimonios` (`id`, `titulo`, `email`, `fecha`, `txt`, `face`, `twit`, `insta`, `linkedin`, `imagen`, `orden`) VALUES
(17, 'Alejandro V.', NULL, NULL, '<p>Simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de rellen&ntilde;o 1500</p>', 'https://www.facebook.com/', 'https://www.twitter.com', 'https://www.instagram.com', 'https://www.linkedin.com', '504884043.jpg', 99),
(18, 'WOZIAL', NULL, NULL, '<p>Simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de rellen&ntilde;o 1500</p>', NULL, NULL, NULL, NULL, '309078405.jpg', 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(100) NOT NULL,
  `user` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `pass` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `nivel` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `user`, `pass`, `fecha`, `nivel`) VALUES
(1, 'efra', '12eb5fef578326a527019871e4ca1c35', '2019-09-16 00:00:00', 2),
(24, 'ecome', '8205f834890064d8ae3622c06c897638', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL,
  `nivel` int(1) NOT NULL DEFAULT 0,
  `distribuidor` int(1) NOT NULL DEFAULT 0,
  `alta` date DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `udate` timestamp NOT NULL DEFAULT current_timestamp(),
  `nombre` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pass` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empresa` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `rfc` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `calle` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `noexterior` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `nointerior` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `entrecalles` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pais` varchar(100) COLLATE utf8_spanish_ci DEFAULT 'Mexico',
  `estado` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `municipio` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colonia` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cp` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `calle2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `noexterior2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `nointerior2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `entrecalles2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pais2` varchar(100) COLLATE utf8_spanish_ci DEFAULT 'Mexico',
  `estado2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `municipio2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colonia2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cp2` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `blogpic`
--
ALTER TABLE `blogpic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `calendario`
--
ALTER TABLE `calendario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `calendariopic`
--
ALTER TABLE `calendariopic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carousel2`
--
ALTER TABLE `carousel2`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `catalogos`
--
ALTER TABLE `catalogos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `catalogospic`
--
ALTER TABLE `catalogospic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galerias`
--
ALTER TABLE `galerias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galeriaspic`
--
ALTER TABLE `galeriaspic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ipn`
--
ALTER TABLE `ipn`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedidosdetalle`
--
ALTER TABLE `pedidosdetalle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productoscat`
--
ALTER TABLE `productoscat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `productoscolor`
--
ALTER TABLE `productoscolor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `productosexistencias`
--
ALTER TABLE `productosexistencias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productosmarcas`
--
ALTER TABLE `productosmarcas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productospic`
--
ALTER TABLE `productospic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productostalla`
--
ALTER TABLE `productostalla`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `productostallaclasif`
--
ALTER TABLE `productostallaclasif`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productostallarel`
--
ALTER TABLE `productostallarel`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos_has_color`
--
ALTER TABLE `productos_has_color`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `blogpic`
--
ALTER TABLE `blogpic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `calendario`
--
ALTER TABLE `calendario`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `calendariopic`
--
ALTER TABLE `calendariopic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `carousel`
--
ALTER TABLE `carousel`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `carousel2`
--
ALTER TABLE `carousel2`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `catalogos`
--
ALTER TABLE `catalogos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `catalogospic`
--
ALTER TABLE `catalogospic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `galerias`
--
ALTER TABLE `galerias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `galeriaspic`
--
ALTER TABLE `galeriaspic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ipn`
--
ALTER TABLE `ipn`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pedidosdetalle`
--
ALTER TABLE `pedidosdetalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `productoscat`
--
ALTER TABLE `productoscat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `productoscolor`
--
ALTER TABLE `productoscolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `productosexistencias`
--
ALTER TABLE `productosexistencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT de la tabla `productosmarcas`
--
ALTER TABLE `productosmarcas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productospic`
--
ALTER TABLE `productospic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `productostalla`
--
ALTER TABLE `productostalla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `productostallaclasif`
--
ALTER TABLE `productostallaclasif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `productostallarel`
--
ALTER TABLE `productostallarel`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos_has_color`
--
ALTER TABLE `productos_has_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
