<?php
$requiereFactura=(isset($_SESSION['requierefactura']))?$_SESSION['requierefactura']:0;
$requiereFacturaIcon=(isset($_SESSION['requierefactura']) AND $_SESSION['requierefactura']==1)?'fas fa-check color-verde':'far fa-square uk-text-muted';


?>
<!DOCTYPE html>
<?=$headGNRL?>
<body>

<div class="uk-container">
  <div class="margin-top-150"></div>
  <?php
  
  //echo '<div class="padding-v-20 bg-dark color-blanco">'.$carroTotalProds.'</div>';
  if ($carroTotalProds>0) {
    echo '
    <form method="post">
      <input type="hidden" name="actualizarcarro" value="1">

      <div class="uk-width-1-1 margin-top-50">
        <div class="uk-panel uk-panel-box">
          <h3 class="uk-text-center"><i class="uk-icon uk-icon-small uk-icon-check-square-o"></i> &nbsp; Productos y cantidades:</h3>

          <table class="uk-table uk-table-striped uk-table-hover uk-table-middle uk-table-responsive uk-text-center">
            <thead>
              <tr>
                <th width="50px"></th>
                <th >Producto</th>
                <th width="100px">Cantidad</th>
                 <th width="100px">Color</th>
                <th width="100px" class="uk-text-right">Precio de lista</th>
                <th width="100px" class="uk-text-right">Precio final</th>
                <th width="100px" class="uk-text-right">Importe</th>
              </tr>
            </thead>
            <tbody>';

            $subtotal=0;
            $num=0;
            if(isset($_SESSION['carro'])){
            
              foreach ($arreglo as $key) {

                $itemId=$key['idExiste'];
                
                $CONSULTA0 = $CONEXION -> query("SELECT * FROM productos_has_color WHERE id = $itemId");
                $row_CONSULTA0 = $CONSULTA0 -> fetch_assoc();
                $prodId=$row_CONSULTA0['id_producto'];
               
                $colorId=$row_CONSULTA0['id_color'];
                $existencias=$row_CONSULTA0['existencias'];

                $CONSULTACOLOR = $CONEXION -> query("SELECT * FROM productoscolor WHERE id = $colorId");
                $rowColor = $CONSULTACOLOR ->fetch_assoc(); 
                
                $CONSULTA = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
                $rowCONSULTA = $CONSULTA -> fetch_assoc();
                $producto=$rowCONSULTA['sku'].' - '.$rowCONSULTA['titulo'];
                   
                $preciocampo = 'precio';

          
                if(isset($ulevel)){
                  $precio='';
                  switch ($ulevel) {
                    case '0':
                      $preciocampo = 'precio';
                      break;
                    case '1':
                      $preciocampo = 'precio1';
                      break;
                    case '2':
                      $preciocampo = 'precio2';
                      break;
                    case '3':
                      $preciocampo = 'precio3';
                      break;
                    
                    default:
                      # code...
                      break;
                  }

                }
              
                $link=$prodId.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($rowCONSULTA['titulo'])))).'-.html';

                $importe=($rowCONSULTA[$preciocampo]*(100-$rowCONSULTA['descuento'])/100)*$key['Cantidad'];
                $subtotal+=$importe;

                echo '
                <tr>
                  <td>
                    <span class="quitar uk-icon-button uk-button-danger" data-id="'.$prodId.'" uk-icon="icon:trash"></span><br>
                  </td>
                  <td class="uk-text-left@m">
                    <a href="#pics" uk-scroll>'.$producto.'</a>
                  </td>
                  <td class="uk-text-right@m">
                    <input type="number" name="cantidad'.$num.'" value="'.$key['Cantidad'].'" min="1" max="'.$existencias.'" class="cantidad uk-input uk-form-width-small input-number uk-text-right" tabindex="10">
                  </td>
                  <td>
                  '.$rowColor['name'].'
                  </td>
                  <td class="uk-text-right@m">
                    '.number_format(($rowCONSULTA[$preciocampo]),2).'
                  </td>
                  <td class="uk-text-right@m">
                    '.number_format(($rowCONSULTA[$preciocampo]*(100-$rowCONSULTA['descuento'])/100),2).'
                  </td>
                  <td class="uk-text-right@m">
                    '.number_format($importe,2).'
                  </td>
                </tr>';

                $num++;
              }
            }

            $envio=$shipping*$carroTotalProds;
            
            $iva=($taxIVA>0)?$subtotal*$taxIVA:0;
            $subtotal=$subtotal+$iva;
            $total=$subtotal+$envio+$shippingGlobal;

            if ($total>0) {
              

              if($taxIVA>0){
                echo '
                <tr>
                  <td colspan="5" class="uk-text-right">
                    IVA
                  </td>
                  <td class="uk-text-right">
                    '.number_format($iva,2).'
                  </td>
                </tr>
                <tr>
                  <td colspan="5" class="uk-text-right">
                    Subtotal
                  </td>
                  <td class="uk-text-right">
                    '.number_format($subtotal,2).'
                  </td>
                </tr>';
              }
              if ($shippingGlobal>0) {
                echo '
                <tr>
                  <td>
                  </td>
                  <td class="uk-text-left@m" colspan="3">
                    Envío global
                  </td>
                  <td class="uk-text-right@m">
                    1
                  </td>
                  <td class="uk-text-right@m">
                    '.number_format($shippingGlobal,2).'
                  </td>
                  <td class="uk-text-right@m">
                    '.number_format($shippingGlobal,2).'
                  </td>
                </tr>';
              }
              if ($shipping>0) {
                echo '
                <tr>
                  <td>
                  </td>
                  <td class="uk-text-left@m" colspan="3">
                    Envío por pieza
                  </td>
                  <td class="uk-text-right@m">
                    '.$carroTotalProds.'
                  </td>
                  <td class="uk-text-right@m">
                    '.number_format($shipping,2).'
                  </td>
                  <td class="uk-text-right@m">
                    '.number_format($envio,2).'
                  </td>
                </tr>';
              }
              if($taxIVA>0){
                echo '
                <tr>
                  <td colspan="6" class="uk-text-right">
                    Total
                  </td>
                  <td class="uk-text-right">
                    '.number_format($total,2).'
                  </td>
                </tr>
                ';
              }else{
                echo '
                <tr>
                  <td colspan="6" class="uk-text-right">
                    Total
                  </td>
                  <td class="uk-text-right">
                    '.number_format($subtotal,2).'
                  </td>
                </tr>';
              }
            }
      echo '
            </tbody>
          </table>
        </div>
      </div>

      <div class="uk-width-1-1 uk-text-center margin-v-50">
        <div uk-grid class="uk-flex-center">
          <div>
            <a href="productos" class="uk-button uk-button-large uk-button-default"><i uk-icon="icon:arrow-left;ratio:1.5;"></i> &nbsp; Seguir comprando</a>
          </div>
          <div>
            <span class="emptycart uk-button uk-button-large uk-button-default"><i uk-icon="icon:trash"></i> &nbsp; Vaciar carrito</span>
          </div>
          <div>
            <button class="uk-button uk-button-personal uk-button-large uk-hidden" id="actualizar">Actualizar &nbsp; <i uk-icon="icon:refresh;ratio:1.5;"></i></button>
            <a href="Revisar_datos_personales" class="uk-button uk-button-large uk-button-personal" id="siguiente">Continuar &nbsp; <i uk-icon="icon:arrow-right;ratio:1.5;"></i></a>
          </div>
        </div>
      </div>
    </form>

    <div uk-grid id="pics">';


    $prodArray[]=0;
    if(isset($_SESSION['carro'])){
      foreach ($arreglo as $key) {
        $itemId=$key['Id'];
        /*$CONSULTA0 = $CONEXION -> query("SELECT * FROM productosexistencias WHERE id = $itemId");
        $row_CONSULTA0 = $CONSULTA0 -> fetch_assoc();
        $prodId=$row_CONSULTA0['producto'];*/
        $prodArray[$itemId]=1;
      }
    }

    foreach ($prodArray as $key => $value) {
      if ($key!=0) {
        $CONSULTA = $CONEXION -> query("SELECT * FROM productos WHERE id = $key");
        $rowCONSULTA = $CONSULTA -> fetch_assoc();
        
        $link=$key.'_'.'productosdetalle';
        
        // Fotos
        $CONSULTA2 = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $key ORDER BY orden");
        $rowCONSULTA2 = $CONSULTA2 -> fetch_assoc();
        $pic='img/contenido/productos/'.$rowCONSULTA2['id'].'.jpg';
        $picHtml=(file_exists($pic))?$pic:$noPic;

        echo '
        <div class="uk-width-1-4">
          <a href="'.$link.'" target="_blank">
            
            <div class="uk-padding-small" style="width:224px; height:300px;">
                  <div class="" style="
                    -webkit-box-shadow: -8px -8px 0px 0px rgba(92,178,49,1);
                    -moz-box-shadow: -8px -8px 0px 0px rgba(92,178,49,1);
                    box-shadow: -8px -8px 0px 0px rgba(92,178,49,1);
                    ">
                    <div class="uk-padding-small" style="-webkit-box-shadow: -3px -3px 15px 0px rgba(0,0,0,0.75);
                      -moz-box-shadow: -3px -3px 15px 0px rgba(0,0,0,0.75);
                      box-shadow: -3px -3px 15px 0px rgba(0,0,0,0.75);">
                      <div class="uk-width-1-1 uk-margin-remove uk-flex uk-flex-center" style="">
                        <div class="uk-width-1-1 uk-padding uk-margin-remove uk-background-contain" 
                        style="height:150px; width:150px;background-image: url('.$pic.');">
                          
                        </div>
                      </div>
                      <div class="uk-width-1-1 uk-flex uk-flex-center uk-padding-remove uk-margin-remove ">
                        <hr class="" style="height:1px;
                          border:solid transparent 1px;
                          background-color: #4d6b05;
                          padding: 0;
                          margin:12px 0 5px 0;
                          max-width:40px;
                          width:100%;">
                      </div>
                      <div class="uk-width-1-1 uk-text-center uk-text-truncate text-11 t-verde">
                        '.$rowCONSULTA['titulo'].'
                      </div>
                    </div>
                  </div>
              </div>

          </a>
        </div>
        ';
      }
    }
  echo '
  </div> <!-- grid -->';

  }else{
    echo '
  <div class="uk-width-1-1 uk-text-center margin-v-50">
    <div class="uk-alert uk-alert-danger text-xl">El carro está vacío</div>
  </div>';
  }
  ?>
</div> <!-- container -->


<div class="uk-width-1-1 uk-text-center margin-top-50">
  &nbsp;
</div>

<?=$footer?>

<?=$scriptGNRL?>

<script type="text/javascript">

  $(".quitar").click(function(){
    var id = $(this).data("id");
    $.ajax({
      method: "POST",
      url: "addtocart",
      data: { 
        id: id,
        cantidad: 0,
        removefromcart: 1
      }
    })
    .done(function() {
      location.reload();
    });
  })

  $(".emptycart").click(function(){
    $.ajax({
      method: "POST",
      url: "emptycart",
      data: { 
        emptycart: 1
      }
    })
    .done(function() {
      location.reload();
    });
  })

  $(".cantidad").keyup(function() {
    var inventario = $(this).attr("data-inventario");
    var cantidad = $(this).val();
    inventario=1*inventario;
    cantidad=1*cantidad;
    if(inventario<=cantidad){
      $(this).val(inventario);
    }
    $("#actualizar").removeClass("uk-hidden");
    $("#siguiente").addClass("uk-hidden");
  })

  $(".cantidad").focusout(function() {
    var inventario = $(this).attr("data-inventario");
    var cantidad = $(this).val();
    console.log(cantidad);
    inventario=1*inventario;
    cantidad=1*cantidad;
    if(inventario<=cantidad){
      console.log(inventario*2+" - "+cantidad);
      $(this).val(inventario);
    }
    $("#actualizar").removeClass("uk-hidden");
    $("#siguiente").addClass("uk-hidden");
  })
</script>
</body>
</html>