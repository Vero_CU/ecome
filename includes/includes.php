<?php
$CONSULTA = $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");
$rowCONSULTA = $CONSULTA -> fetch_assoc();

$stickerClass=($carroTotalProds==0 OR $identificador==500 OR $identificador==501 OR $identificador==502)?'uk-hidden':'';
/* %%%%%%%%%%%%%%%%%%%% MENSAJES               */
	if($mensaje!=''){
		$mensajes='
			<div class="uk-container">
				<div uk-grid>
					<div class="uk-width-1-1 margin-v-20">
						<div class="uk-alert-'.$mensajeClase.'" uk-alert>
							<a class="uk-alert-close" uk-close></a>
							'.$mensaje.'
						</div>					
					</div>
				</div>
			</div>';
	}

/* %%%%%%%%%%%%%%%%%%%% RUTAS AMIGABLES        */
		$rutaInicio			=	'Inicio';
		$rutaProductos		=	'productos';
		$rutaProductosDetalle=	'productosdetalle';
		$rutaNosotros		=	'nosotros';
		$rutaContacto		=	'contacto';
		$rutaPedido			=	'revisar_orden';
		$rutaPedido2		=	$ruta.'revisar_datos_personales';
		$rutaMiCta          =   'mi-cuenta';

/* %%%%%%%%%%%%%%%%%%%% MENU                   */
	$menu='
		<li class="'.$nav1.'"><a class="" href="Inicio">Inicio</a></li>
		';

	$menuMovil='
		<li><a 
			style="background-color:'.$rowCONSULTA["color1"].';"
			class="uk-text-center  cero-middle '.$nav1.'" href="'.$ruta.'">Inicio</a></li>
		<li><a 
			style="background-color:'.$rowCONSULTA["color1"].';"
			class="uk-text-center  cero-middle '.$nav2.'" href="'.$rutaNosotros.'">Nosotros</a></li>
		<li><a 
			style="background-color:'.$rowCONSULTA["color1"].';"
			class="uk-text-center  cero-middle '.$nav2.'" href="'.$rutaProductos.'">Productos</a></li>
		<li><a 
			style="background-color:'.$rowCONSULTA["color1"].';"
			class="uk-text-center  cero-middle" href="'.$rutaContacto.'">Contacto</a></li>

		<br><br>
		<li class="uk-hidden@m uk-padding cero-middle">
			
				<div class="uk-width-1-1 uk-search uk-search-default uk-button-white uk-padding uk-margin-remove uk-flex uk-flex-center uk-flex-middle" 
				style="height:50px;padding:0;margin:0;background-color:#567508;">
			        <a href="" class="uk-width-1-1 uk-search-icon-flip uk-padding-remove uk-margin-remove" uk-search-icon style="font-size:16px;padding:0;margin:0;"></a>
			        <input class="uk-search-input uk-padding-small uk-margin-remove search" type="text" placeholder="Buscar..." style="font-size:14px;border:solid transparent;padding:0;margin:0;height:46px;">
			    </div>
			
		</li>
		<li class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-flex uk-flex-center uk-flex-middle uk-hidden@m" style="color:#000!important">
			'.$loginButton.'
		</li>
		<li class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-text-center uk-flex uk-flex-center uk-flex-middle uk-grid uk-hidden@m" uk-grid>';
			if(strlen($socialWhats) > 4){
			$menuMovil.= '
				<div class="uk-width-auto uk-padding-remove uk-margin-remove uk-text-center uk-flex uk-flex-center uk-flex-middle social">
					<a href="'.$socialWhats.'?>" target="_blank" 
					style="border: solid 1px transparent;color:#000;">
						<span uk-icon="icon: whatsapp; ratio: 1.2"></span>
					</a>
				</div>';
			}
			if(strlen($socialFace) > 4){
			$menuMovil.= '
				<div class="uk-width-auto uk-padding-remove uk-margin-remove uk-text-center uk-flex uk-flex-center uk-flex-middle social">
					<a href="'.$socialFace.'?>" target="_blank" 
					style="border: solid 1px transparent;color:#000;">
						<span uk-icon="icon: facebook; ratio: 1.2"></span>
					</a>
				</div>';
			}
			if(strlen($socialInst) > 4){
			$menuMovil.= '
				<div class="uk-width-auto uk-padding-remove uk-margin-remove uk-text-center uk-flex uk-flex-center uk-flex-middle social">
					<a href="'.$socialInst.'?>" target="_blank" 
					style="border: solid 1px transparent;color:#000;">
						<span uk-icon="icon: instagram; ratio: 1.2"></span>
					</a>
				</div>';
			}
			$menuMovil.= '
		</li>
		
						
		';

/* %%%%%%%%%%%%%%%%%%%% HEADER                 */

			
			
	$header='
		<div class="uk-offcanvas-content uk-position-relative">

			<div class="uk-position-fixed uk-width-1-1 uk-height-viewport bg-claro z1 uk-flex uk-flex-center uk-flex-middle" id="preloader">
				<div class="claro" uk-spinner="ratio: 5">
				</div>
			</div>

			<header class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-visible@m" style="background-image:url(/img/design/bg_header.png)">
				<div class="uk-container uk-container-expand">
					<div uk-grid class="uk-grid-match uk-margin-remove padding-h-40">
						<div class="uk-width-2-5 uk-padding-remove uk-margin-remove uk-flex uk-flex-center uk-flex-middle uk-grid" uk-grid>
							<!-- Botón menú móviles -->
							<div class="uk-width-auto uk-padding-remove uk-margin-remove uk-flex uk-flex-center uk-flex-middle">
								<a href="#menu-movil" uk-toggle class="uk-button color-primary uk-padding-remove uk-margin-remove">
									<i class="uk-margin-remove fa fa-bars" aria-hidden="true" style="padding-top:10px"></i> &nbsp; 
								</a>
							</div>
							<div class="uk-width-expand uk-padding-small uk-margin-remove uk-flex uk-flex-center uk-flex-middle">
								<div class="uk-search uk-search-default uk-button-white uk-padding-remove uk-margin-remove" 
								style="line-height:0">
							        <a href="" class="uk-search-icon-flip uk-padding-remove uk-margin-remove" uk-search-icon style="font-size:16px;padding:0;margin:0"></a>
							        <input class="uk-search-input uk-padding-remove uk-margin-remove search" type="text" placeholder="Buscar..." style="font-size:14px;height: 30px;padding:0;margin:0">
							    </div>
							</div>
						</div>
						
						<div class="uk-width-1-5 uk-padding-remove">
							<a href="Inicio">
								<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-background-contain uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/design/logo.png);margin-top: -10px!important;height:120%;"> </div>
							</a>
						</div>
						<div class="uk-width-2-5 uk-padding-remove uk-margin-remove uk-grid" uk-grid>
							<!-- Botón menú móviles -->
							<div class="uk-width-expand uk-padding-remove uk-margin-remove uk-flex uk-flex-center uk-flex-middle">
								<div uk-grid class="uk-width-1-1 uk-grid-collapse uk-padding-remove uk-aling-right">
									'.$loginButton.'
								</div>
							</div>
							<div class="uk-width-auto uk-padding-remove uk-margin-remove uk-text-center 
							uk-flex uk-flex-center uk-flex-middle uk-grid" uk-grid>';
								if(strlen($socialWhats) > 4){
								$header.= '
								<div class="uk-width-auto uk-margin-remove uk-text-center social">
									<a href="'.$socialWhats.'" target="_blank">
										<span uk-icon="icon: whatsapp; ratio: 1.2"></span>
									</a>
								</div>';
								}
								if(strlen($socialFace) > 4){
								$header.= '
								<div class="uk-width-auto uk-margin-remove uk-text-center social">
									<a href="'.$socialFace.'" target="_blank">
										<span uk-icon="icon: facebook; ratio: 1.2"></span>
									</a>
								</div>';
								}
								if(strlen($socialInst) > 4){
								$header.= '<div class="uk-width-auto uk-margin-remove uk-text-center social">
									<a href="'.$socialInst.'" target="_blank">
										<span uk-icon="icon: instagram; ratio: 1.2"></span>
									</a>
								</div>';
								}
								$header.= '<div class="uk-width-auto uk-margin-remove uk-text-centerl social">
									<a href="revisar_orden">
										<span uk-icon="icon: cart; ratio: 1.2"></span>
										';
										if($carroTotalProds > 0){
										$header.='<div class="uk-margin-remove uk-flex uk-flex-center uk-flex-middle precio">
											<span class="uk-padding-remove uk-margin-remove uk-text-center t-negro social" id="cartcount">'.$carroTotalProds.'</span>
										</div>';
										}
										$header.='
									</a>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</header>
			<div class="uk-width-expand uk-padding-remove uk-margin-remove" id="menu-movil" uk-offcanvas="mode: push;overlay: true">
				<div class="uk-width-1-1 uk-offcanvas-bar uk-flex uk-flex-center uk-flex-middle uk-grid
				uk-margin-remove uk-padding-remove uk-grid" uk-grid>
					<button class="uk-offcanvas-close" type="button" uk-close></button>
					<div class="uk-width-1-2@m uk-flex uk-flex-center uk-flex-middle uk-padding-remove" 
					style="height:100vh; background-image: url(./img/contenido/varios/'.$rowCONSULTA["imagen4"].');background-repeat:no-repeat;background-size: cover;">
						<div class="uk-flex uk-flex-center uk-flex-middle" 
						style="background-color:'.$rowCONSULTA["color1"].';height:100vh;width:100%">
							<ul class="uk-width-1-1 uk-nav uk-nav-primary uk-nav-parent-icon uk-nav-center uk-margin-auto-vertical uk-padding-large" uk-nav>
								'.$menuMovil.'
							</ul>
						</div>		
					</div>
					<div class="uk-width-1-2@m uk-visible@m uk-flex uk-flex-center uk-flex-middle" style="background-color:#fff;height:100vh; ">
						<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-background-contain uk-flex uk-flex-center uk-flex-middle" 
						style="background-image: url(./img/design/logo.png);height:140px;"> </div>
					</div>
				</div>
			</div>
			<header class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-hidden@m" style="background-image:url(/img/design/bg_header.png); height:50px;">
				<div class="uk-container uk-container-expand uk-padding-remove uk-margin-remove">
					<div uk-grid class="uk-grid-match uk-margin-remove" style="padding:5px 15px">
						<div class="uk-width-auto uk-padding-remove uk-margin-remove uk-flex uk-flex-center uk-flex-middle uk-grid" uk-grid style="position:fixed;right:0;bottom:0;">
							<!-- Botón menú móviles -->
							<div class="uk-width-auto uk-padding-small uk-margin-remove uk-flex uk-flex-center uk-flex-middle">
								<a href="#menu-movil" uk-toggle class="uk-button color-primary uk-padding-remove uk-margin-remove">
									<i class="uk-padding-remove uk-margin-remove fa fa-bars" aria-hidden="true"></i> &nbsp; 
								</a>
							</div>
						</div>
						
						<div class="uk-width-auto uk-padding-remove uk-margin-remove">
							<a class="uk-padding-remove uk-margin-remove" href="Inicio">
								<img  class="uk-padding-remove uk-margin-remove" src="./img/design/logo.png" style="max-width:100px">
								
							</a>
						</div>
						<div class="uk-width-expand"></div>
						<div class="uk-width-auto uk-padding-remove uk-margin-remove uk-grid" uk-grid>
							<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-text-center
							uk-flex uk-flex-center uk-flex-middle uk-grid social" uk-grid>
									<span uk-icon="icon: cart; ratio: 1.2" style="padding-top:10px"></span>
									';
									if($carroTotalProds > 0){
									$header.='<div class="uk-margin-remove uk-flex uk-flex-center uk-flex-middle precio" style="top:10px; margin-left: 24px!important;">
										<span class="uk-padding-remove uk-margin-remove uk-text-center t-negro social" id="cartcount">'.$carroTotalProds.'</span>
									</div>';
									}
									$header.='
							</div>
						</div>
						
					</div>
				</div>
			</header>

			<div id="cotizacion-fixed" class="uk-position-top uk-height-viewport uk-padding-remove uk-margin-remove '.$stickerClass.'">
				<div class="uk-padding-remove uk-margin-remove">
					<a class=" uk-padding-remove uk-margin-remove" href="'.$rutaPedido.'">
						<img class="uk-padding-remove uk-margin-remove" src="img/design/checkout.png"></a>
				</div>
			</div>

			'.$mensajes.'

			<!-- Menú móviles
			<div id="menu-movil" uk-offcanvas="mode: push;overlay: true">
				<div class="uk-offcanvas-bar uk-flex uk-flex-column">
					<button class="uk-offcanvas-close" type="button" uk-close></button>
					<ul class="uk-nav uk-nav-primary uk-nav-parent-icon uk-nav-center uk-margin-auto-vertical" uk-nav>
						'.$menuMovil.'
					</ul>
				</div>
			</div> -->';

/* %%%%%%%%%%%%%%%%%%%% FOOTER                 */
	$whatsIconClass=(isset($_SESSION['whatsappHiden']))?'':'uk-hidden';
	$footer = '
		<footer class="uk-width-1-1 uk-padding-remove uk-margin-remove
		uk-flex uk-flex-center uk-flex-middle" uk-grid>
			<div class="uk-width-1-1 footer">
				<div class="uk-container">
					<div class="uk-padding-remove uk-margin-remove uk-flex uk-flex-center uk-flex-middle" uk-grid>
						<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-flex uk-flex-center uk-flex-middle">
							<a href="'.$ruta.'"><img class="uk-padding-remove" src="img/design/logo_n.png"></a>
						</div>
						<div class="uk-width-1-1 uk-width-1-3@m uk-padding uk-margin-remove">
							<div class="uk-padding-remove uk-margin-remove uk-text-uppercase text-9 t-negro">
								<b>NAVEGACIÓN</b>
							</div>
							<div class="uk-padding-remove uk-margin-remove">
								<hr class="hr-footer">
							</div>
							<div class="uk-padding-remove uk-margin-remove">
								<ul class="uk-padding-remove uk-margin-remove">
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="'.$rutaInicio.'#nosotros">NOSOTROS</a>
									</li>
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="'.$rutaProductos.'">PRODUCTOS</a>
									</li>
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="'.$rutaInicio.'#contacto">CONTACTO</a>
									</li>
								</ul>
							</div>
							<div class="uk-padding-remove uk-margin-remove">
								<hr class="hr-footer">
							</div>
						</div>
						<div class="uk-width-1-1 uk-width-1-3@m uk-padding uk-margin-remove">
							<div class="uk-padding-remove uk-margin-remove uk-text-uppercase text-9 t-negro">
								<b>AYUDA</b>
							</div>
							<div class="uk-padding-remove uk-margin-remove">
								<hr class="hr-footer">
							</div>
							<div class="uk-padding-remove uk-margin-remove">
								<ul class="uk-padding-remove uk-margin-remove">
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="1_politicas">'.$tyct1.'</a>
									</li>
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="preguntas-frecuentes">PREGUNTAS FRECUENTES</a>
									</li>
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="2_politicas">'.$tyct2.'</a>
									</li>
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="3_politicas">'.$tyct3.'</a>
									</li>
								</ul>
							</div>
							<div class="uk-padding-remove uk-margin-remove">
								<hr class="hr-footer">
							</div>
						</div>
						<div class="uk-width-1-1 uk-width-1-3@m uk-padding uk-margin-remove">
							<div class="uk-padding-remove uk-margin-remove uk-text-uppercase text-9 t-negro">
								<b>SOCIAL</b>
							</div>
							<div class="uk-padding-remove uk-margin-remove">
								<hr class="hr-footer">
							</div>
							<div class="uk-padding-remove uk-margin-remove">
								<ul class="uk-padding-remove uk-margin-remove">
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="'.$rutaMiCta.'" target="_blank">
											MI CUENTA
										</a>
									</li>';
								if(strlen($socialFace) > 4){
								$footer.= '
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="'.$socialFace.'" target="_blank">
											FACEBOOK
										</a>
									</li>';
								}
								if(strlen($socialInst) > 4){
								$footer.= '
									<li class="uk-padding-remove uk-margin-remove footer-li">
										<a class="uk-text-uppercase text-9" href="'.$socialInst.'" target="_blank">
											INSTAGRAM
										</a>
									</li>';
								}
								
								$footer.= '
									<div class="uk-width-auto uk-padding-remove uk-margin-remove uk-text-center uk-grid" uk-grid>
										<div class="uk-width-auto uk-padding-small uk-margin-remove uk-text-center">
											<a class=" text-9" href="'.$socialWhats.'" target="_blank">
												<span uk-icon="icon: whatsapp; ratio: 1.2"></span>
											</a>
										</div>';
										
										if(strlen($socialFace) > 4){
										$footer.= '
										<div class="uk-width-auto uk-padding-small uk-margin-remove uk-text-center">
											<a class=" text-9" href="'.$socialFace.'" target="_blank">
												<span uk-icon="icon: facebook; ratio: 1.2"></span>
											</a>
										</div>
										';
										}
										if(strlen($socialInst) > 4){
										$footer.= '
										<div class="uk-width-auto uk-padding-small uk-margin-remove uk-text-center">
											<a class=" text-9" href="'.$socialInst.'" target="_blank">
												<span uk-icon="icon: instagram; ratio: 1.2"></span>
											</a>
										</div>';
										}
										$footer.= '
									</div>
								</ul>
							</div>
							<div class="uk-padding-remove uk-margin-remove">
								<hr class="hr-footer">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="uk-width-1-1 uk-padding-remove uk-margin-remove
		uk-flex uk-flex-center uk-flex-middle bg-footer" style="z-index: 0;">
				<div class="uk-container uk-position-relative">
					<div class="uk-width-1-1 uk-text-center">
						<div class="uk-padding-small uk-text-uppercase" style="color:#000">
							<div class="text-9 uk-padding-remove uk-margin-remove"
							style="letter-spacing: 2px;">TODOS LOS DERECHOS RESERVADOS ECOME '.date('Y').'</div>
							<div class="text-9 uk-padding-remove uk-margin-remove" 
							style="font-weight:bold;letter-spacing: 2px;"> DISEÑO POR <a href="https://wozial.com/" target="_blank" class="color-negro">WOZIAL MARKETINGS LOVERS</a></div>
						</div>
					</div>
				</div>
			</div>
		</footer>

		

		'.$loginModal.'

		<!--<div id="whatsapp-plugin" class="uk-hidden">
			<div id="whats-head" class="uk-position-relative">
				<div uk-grid class="uk-grid-small uk-grid-match">
					<div>
						<div class="uk-flex uk-flex-center uk-flex-middle">
							<img class="uk-border-circle padding-10" src="img/design/logo-og.jpg" style="width:70px;">
						</div>
					</div>
					<div>
						<div class="uk-flex uk-flex-center uk-flex-middle color-blanco">
							<div>
								<span class="text-sm">'.$Brand.'</span><br>
								<span class="text-6 uk-text-light">Atención en línea vía chat</span>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-position-right color-blanco text-sm">
					<span class="pointer padding-10" id="whats-close">x</spam>
				</div>
			</div>
			<div id="whats-body-1" class="uk-flex uk-flex-middle">
				<div class="bg-white uk-border-rounded padding-h-10" style="margin-left:20px;">
					<img src="img/design/loading.gif" style="height:40px;">
				</div>
			</div>
			<div id="whats-body-2" class="uk-hidden">
				<span class="uk-text-bold uk-text-muted">'.$Brand.'</span><br>
				Hola 👋<br>
				¿Cómo puedo ayudarte?
			</div>
			<div id="whats-footer" class="uk-flex uk-flex-center uk-flex-middle">
				<a href="'.$socialWhats.'" target="_blank" class="uk-button uk-button-small" id="button-whats"><i class="fab fa-whatsapp fa-lg"></i> <span style="font-weight:400;">Comenzar chat</span></a>
			</div>
		</div>
		<div id="whats-show" class="'.$whatsIconClass.' pointer uk-border-circle color-white uk-box-shadow-large" style="background-color: rgb(9, 94, 84);">
			<i class="fab fa-3x fa-whatsapp"></i>
		</div>-->
	</div>

	<div id="spinnermodal" class="uk-modal-full" uk-modal>
		<div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle uk-height-viewport">
			<div>
				<div class="claro" uk-spinner="ratio: 5">
				</div>
			</div>
		</div>
   	</div>';

/* %%%%%%%%%%%%%%%%%%%% HEAD GENERAL                */
	$headGNRL='
		<html lang="'.$languaje.'">
		<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">

			<meta charset="utf-8">
			<title>'.$title.'</title>
			<meta name="description" content="'.$description.'" />
			<meta property="fb:app_id" content="'.$appID.'" />
			<link rel="image_src" href="'.$ruta.$logoOg.'" />

			<meta property="og:type" content="website" />
			<meta property="og:title" content="'.$title.'" />
			<meta property="og:description" content="'.$description.'" />
			<meta property="og:url" content="'.$rutaEstaPagina.'" />
			<meta property="og:image" content="'.$ruta.$logoOg.'" />

			<meta itemprop="name" content="'.$title.'" />
			<meta itemprop="description" content="'.$description.'" />
			<meta itemprop="url" content="'.$rutaEstaPagina.'" />
			<meta itemprop="thumbnailUrl" content="'.$ruta.$logoOg.'" />
			<meta itemprop="image" content="'.$ruta.$logoOg.'" />

			<meta name="twitter:title" content="'.$title.'" />
			<meta name="twitter:description" content="'.$description.'" />
			<meta name="twitter:url" content="'.$rutaEstaPagina.'" />
			<meta name="twitter:image" content="'.$ruta.$logoOg.'" />
			<meta name="twitter:card" content="summary" />

			<meta name="viewport"       content="width=device-width, initial-scale=1">

			<link rel="icon"            href="'.$ruta.'img/design/favicon.ico" type="image/x-icon">
			<link rel="shortcut icon"   href="img/design/favicon.ico" type="image/x-icon">
			<link rel="stylesheet"      href="https://cdn.jsdelivr.net/npm/uikit@'.$uikitVersion.'/dist/css/uikit.min.css" />
			<link rel="stylesheet/less" href="css/general.less" >
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Lato:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
			
			<!-- jQuery is required -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

			<!-- UIkit JS -->
			<script src="https://cdn.jsdelivr.net/npm/uikit@'.$uikitVersion.'/dist/js/uikit.min.js"></script>
			<script src="https://cdn.jsdelivr.net/npm/uikit@'.$uikitVersion.'/dist/js/uikit-icons.min.js"></script>

			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.8/fullpage.css" />

			<!-- FULL PAGE -->
			<!-- Esta línea es opcional. Sólamente es necesaria si se hace uso de la opción `css3:false` y se quiere usar otro efecto `easing` en lugar de `linear`, `swing` o `easeInOutCubic`. -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.8/vendors/easings.min.js"></script>

			<!-- Esta línea es opcional y sólamente es necesaria en caso de usar la opción `scrollOverflow:true`. -->
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.8/vendors/scrolloverflow.min.js"></script>

			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.8/fullpage.js"></script>
			<!-- FULL PAGE -->

			<!-- Font Awesome -->
			<script src="https://kit.fontawesome.com/910783a909.js" crossorigin="anonymous"></script>

			<!-- Less -->
			<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>
		</head>';

/* %%%%%%%%%%%%%%%%%%%% SCRIPTS                */
	$scriptGNRL='
		<script src="js/general.js"></script>

		<!--script src="//code.jivosite.com/widget.js" data-jv-id="R4ZWEOn0XH" async></script-->

		<script>
			$(".cantidad").keyup(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					$(this).val(inventario);
				}
				console.log(inventario+" - "+cantidad);
			})
			$(".cantidad").focusout(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					//console.log(inventario*2+" - "+cantidad);
					$(this).val(inventario);
				}
			})

			// Agregar al carro
			$(".buybutton").click(function(){
				var id=$(this).data("id");
				var existencias = $(this).data("existencias");
				var cantidad= $("#"+existencias).val();
				console.log("cantidad",cantidad);
				console.log("id",id);
				console.log("existencias", existencias);
				$.ajax({
					method: "POST",
					url: "addtocart",
					data: { 
						id: id,
						cantidad: cantidad,
						existencias:existencias,
						addtocart: 1
					}
				})
				.done(function( msg ) {
					datos = JSON.parse(msg);
					UIkit.notification.closeAll();
					UIkit.notification(datos.msg);
					$("#cartcount").html(datos.count);
					$("#cotizacion-fixed").removeClass("uk-hidden");
				});
			})
		</script>
		';

	// Script login Facebook
	$scriptGNRL.=(!isset($_SESSION['uid']) AND $dominio != 'localhost' AND isset($facebookLogin))?'
		<script>
			// Esta es la llamada a facebook FB.getLoginStatus()
			function statusChangeCallback(response) {
				if (response.status === "connected") {
					procesarLogin();
				} else {
					console.log("No se pudo identificar");
				}
			}

			// Verificar el estatus del login
			function checkLoginState() {
				FB.getLoginStatus(function(response) {
					statusChangeCallback(response);
				});
			}

			// Definir características de nuestra app
			window.fbAsyncInit = function() {
				FB.init({
					appId      : "'.$appID.'",
					xfbml      : true,
					version    : "v3.2"
				});
				FB.AppEvents.logPageView();
			};

			// Ejecutar el script
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/es_LA/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, \'script\', \'facebook-jssdk\'));
			
			// Procesar Login
			function procesarLogin() {
				FB.api(\'/me?fields=id,name,email\', function(response) {
					console.log(response);
					$.ajax({
						method: "POST",
						url: "includes/acciones.php",
						data: { 
							facebooklogin: 1,
							nombre: response.name,
							email: response.email,
							id: response.id
						}
					})
					.done(function( response ) {
						console.log( response );
						datos = JSON.parse( response );
						UIkit.notification.closeAll();
						UIkit.notification(datos.msj);
						if(datos.estatus==0){
							location.reload();
						}
					});
				});
			}
		</script>

		':'';


// Reportar actividad
	$scriptGNRL.=(!isset($_SESSION['uid']))?'':'
		<script>
			var w;
			function startWorker() {
			  if(typeof(Worker) !== "undefined") {
			    if(typeof(w) == "undefined") {
			      w = new Worker("js/activityClientFront.js");
			    }
			    w.onmessage = function(event) {
					//console.log(event.data);
			    };
			  } else {
			    document.getElementById("result").innerHTML = "Por favor, utiliza un navegador moderno";
			  }
			}
			startWorker();
		</script>
		';

/* %%%%%%%%%%%%%%%%%%%% BUSQUEDA               */
	$scriptGNRL.='
		<script>
			$(document).ready(function(){
				$(".search").keyup(function(e){
					console.log("entre js search");
					if(e.which==13){
						var consulta=$(this).val();
						var l = consulta.length;
						if(l>=2){
							window.location = ("'.$ruta.'"+consulta+"_resultado");
						}else{
							UIkit.notification.closeAll();
							UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
						}
					}
				});
			});
		</script>';

/* %%%%%%%%%%%%%%%%%%%% WHATSAPP PLUGIN               */
	$scriptGNRL.=(isset($_SESSION['whatsappHiden']))?'':'
		<script>
			setTimeout(function(){
				$("#whatsapp-plugin").addClass("uk-animation-slide-bottom-small");
				$("#whatsapp-plugin").removeClass("uk-hidden");
			},1000);
			setTimeout(function(){
				$("#whats-body-1").addClass("uk-hidden");
				$("#whats-body-2").removeClass("uk-hidden");
			},6000);
		</script>
			';

	$scriptGNRL.='
		<script>
			$("#whats-close").click(function(){
				$("#whatsapp-plugin").addClass("uk-hidden");
				$("#whats-show").removeClass("uk-hidden");
				$.ajax({
					method: "POST",
					url: "includes/acciones.php",
					data: { 
						whatsappHiden: 1
					}
				})
				.done(function( msg ) {
					console.log(msg);
				});
			});
			$("#whats-show").click(function(){
				$("#whatsapp-plugin").removeClass("uk-hidden");
				$("#whats-show").addClass("uk-hidden");
				$("#whats-body-1").addClass("uk-hidden");
				$("#whats-body-2").removeClass("uk-hidden");
				$.ajax({
					method: "POST",
					url: "includes/acciones.php",
					data: { 
						whatsappShow: 1
					}
				})
				.done(function( msg ) {
					console.log(msg);
				});
			});
		</script>';

