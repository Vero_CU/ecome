<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<div class="padding-top-100"></div>
<div class="uk-width-1-1 uk-margin-remove uk-padding-remove">
	<div class="uk-container uk-container-expand uk-margin-remove uk-padding uk-grid" uk-grid>
<?php
	$CONSULTA_categorias = $CONEXION -> query("SELECT * FROM productoscat");
	$i = 0;
	$numCat = $CONSULTA_categorias -> num_rows;
	while ($rowCategorias = $CONSULTA_categorias -> fetch_assoc()):
		if(!isset($catId) AND $i == 0){
			$catId = $rowCategorias["id"];
		}
		
		$cats = $rowCategorias["txt"];
		$descripcion = $rowCategorias["descripcion"];
		$i++;
?>
	<?php if($numCat > 1): ?>
		<div class="uk-width-1-4 uk-padding-small">
			<div class="uk-padding-remove uk-margin-remove uk-grid" uk-grid style="height: auto!important;">
				<a href="<?= $rowCategorias["id"] ?>_productos.php" class="uk-width-1-1 uk-button uk-button-negro uk-text-uppercase uk-text-light buybutton" data-id="<?=$arrayList[$i]['id']?>" 
				style="" 
				id="footersend">
					<b><?= $cats ?></b>
				</a>
			</div>
			<!--a class="uk-width-1-1" href="<?= $rowCategorias["id"] ?>_productos.php">
				<div class="uk-width-1-1 uk-padding-remove cont-prod" style="height: auto!important;">
					<div class="uk-padding-small sombra"><b><?= $cats ?></b></div>
				</div>
			</a-->
		</div>
	<?php endif ?>
	
<?php endwhile ?>
</div>
</div>

<?php
	$CONSULTA_cat = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $catId LIMIT 1");
	$rowCat = $CONSULTA_cat -> fetch_assoc();
	$arrayList = array();
	$consultaProductos = $CONEXION -> query("SELECT * FROM productos WHERE categoria = $catId AND estatus = 1 ");
	while($rowProductos = $consultaProductos -> fetch_assoc()){
		$prodId = $rowProductos["id"];
		$precio='';

	    $preciocampo = 'precio';
	    $descuento = $rowProductos['descuento'];

		$precio = ($descuento>0)?'
	      <strike style="font-weight: 600;">$'.number_format(($rowProductos[$preciocampo]),2).'</strike><span> / </span><span>$'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2).'</span>
	      ':'
	      $'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2);

	      $rowProductos['precio']=$precio;

		$consultaPic = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $prodId  ORDER BY orden LIMIT 1");
		$pic = $consultaPic -> fetch_assoc();
		$picImg = $pic["id"].".jpg";
		$rowProductos["imagen"] = $picImg;
		array_push($arrayList, $rowProductos);
	}
?>


<div class="uk-width-1-1 uk-margin-remove uk-padding-remove">
	<div class="uk-container uk-container-expand uk-margin-remove uk-padding">
		<div class="uk-grid uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-grid" uk-grid>
				<div class="uk-width-1-1 uk-margin-remove uk-padding t-verde text-xxxl padding-h-40">
					<?= $rowCat["txt"] ?>
				</div>
				<div class="uk-width-1-1 uk-margin-remove padding-h-40">
					<hr class="ht-prods">
				</div>
				<div class="uk-width-1-1 uk-width-1-2@m uk-margin-remove uk-padding t-verde padding-h-40" style="padding-bottom:50px">
					<?= $rowCat["descripcion"] ?> 
				</div>
				<div class="uk-width-1-2 uk-margin-remove uk-padding">
				</div>
			</div>

			<?php 
				for($i = 0; sizeof($arrayList) > $i; $i++):
					$prodId = $arrayList[$i]["id"];
			?>
			<div class="uk-width-1-1 uk-width-1-5@m uk-flex uk-flex-center uk-flex-middle uk-padding uk-margin-remove uk-grid list" uk-grid>
				<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-grid cont-prod prod-height" uk-grid>
					<div class="uk-width-1-1 uk-padding-small uk-margin-remove sombra">
						<a class="uk-width-1-1 uk-padding-remove uk-margin-remove" href="<?=$prodId ?>_productosdetalle">
							<div class="uk-width-1-1 uk-margin-remove pad-img uk-flex uk-flex-center uk-flex-middle">
								<img class="uk-padding-small uk-margin-remove" 
				                style="width:auto;min-height: 175px;" 
				                src="./img/contenido/productos/<?=$arrayList[$i]["imagen"]?>" alt="">
								
								<!--div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-background-cover uk-flex uk-flex-center uk-flex-middle img" style="background-image: url(./img/contenido/productos/<?=$arrayList[$i]["imagen"]?>);"></div-->
							</div>
							<div class="uk-width-1-1 uk-margin-remove uk-padding-remove uk-flex uk-flex-center">
								<hr class="hr-img">
							</div>
							<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-text-center uk-text-truncate t-verde">
								<?= $arrayList[$i]["titulo"] ?>
							</div>
							<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-text-center text-9 t-negro">
								<?= $arrayList[$i]["precio"] ?>
							</div>
						</a>
					</div>
				</div>
				<div class="uk-padding-remove uk-margin-remove pad-compra">
					<br>
					<div> 
						<input type="hidden" min="1" name="" id="<?=$arrayList[$i]['id']?>" value="1">
					</div>
					<a href="" class="uk-button uk-button-negro uk-text-uppercase uk-text-light buybutton" data-id="<?=$arrayList[$i]['id']?>" id="footersend">COMPRA AHORA</a>
				</div>
			</div>
			
			<?php endfor ?>
		</div>
	</div>
</div>


<?=$footer?>

<?=$scriptGNRL?>

</body>
</html>