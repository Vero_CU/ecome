<!DOCTYPE html>
<?=$headGNRL?>
<body>
<script type="text/javascript">
	
</script>

<?=$header?>
<?php 
	$arrayList = array();
	$id=1;
	$CONSULTA = $CONEXION -> query("SELECT * FROM configuracion WHERE id = $id");
	$rowCONSULTA = $CONSULTA -> fetch_assoc();

	$consultaProductos = $CONEXION -> query("SELECT * FROM productos WHERE rand() AND inicio = 1 AND estatus = 1");
	while($rowProductos = $consultaProductos -> fetch_assoc()){
		$prodId = $rowProductos["id"];
		$consultaPic = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $prodId  ORDER BY orden LIMIT 1");
		$pic = $consultaPic -> fetch_assoc();
		$picImg = $pic["id"].".jpg";
		$rowProductos["imagen"] = $picImg;

	
	    $preciocampo = 'precio';

		$descuento = $rowProductos['descuento'];

		if(isset($ulevel)){
			$precio='';
			switch ($ulevel) {
				case '0':
					$preciocampo = 'precio';
					break;
				case '1':
					$preciocampo = 'precio1';
					break;
				case '2':
					$preciocampo = 'precio2';
					break;
				case '3':
					$preciocampo = 'precio3';
					break;
				
				default:
					# code...
					break;
			}

		}

		$precio = ($descuento>0)?'
	      <strike style="font-weight: 600;">$'.number_format(($rowProductos[$preciocampo]),2).'</strike><span> / </span><span>$'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2).'</span>
	      ':'
	      $'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2);

	      $rowProductos['precio']=$precio;

		array_push($arrayList, $rowProductos);

	}
?>

<div id="fullpage">
	<div id="section1" class="section active uk-width-1-1 uk-margin-remove uk-padding-remove un-container uk-container-expand height-100" uk-grid>
		<?= carousel("carousel") ?>
	</div>

	<div id="section2" class="section uk-margin-remove uk-padding-remove height-100" >
		<div class="uk-container uk-container-expand uk-margin-remove uk-padding-remove" id="">
			<div class="uk-grid uk-margin-remove uk-padding-remove" uk-grid>
				<div class="uk-width-1-2@m uk-width-1-1 uk-margin-remove uk-padding uk-flex uk-flex-middle movil-1-1 seccion-padding height-100-intro">
					<div class="nosotros div-intro uk-flex uk-flex-middle" style="padding-top:76px!important;">
					<div>
					
						<div class="uk-width-1-1 uk-padding" id="nosotros" name="nosotros" style="float:right;">
							<img class="uk-padding-remove" src="img/contenido/varios/<?=$nosotros0?>" style="float:right;margin-bottom:40px">
						</div>
						<div class="uk-width-1-1 uk-padding uk-padding-remove@s uk-margin-remove margin-top-20" style="padding-left: 0;">
							<div class="text-xl text-9 t-negro">
								<?=$nosotrost1?>
							</div>
							<div class="uk-padding-remove">
								<hr class="hr-title">
							</div>
							<div class="text-9 text-9 t-negro frase_nosotros">
								<?=$nosotros1?>
							</div>
							<div class="uk-padding-remove">
								<hr class="hr-bottom">
							</div>
						</div>
						<div class="uk-width-1-1 uk-padding uk-padding-remove@s uk-margin-remove" style="padding-left: 0;">
							<div class="text-xl text-9 t-negro">
								<?=$nosotrost2?>
							</div>
							<div class="uk-padding-remove">
								<hr class="hr-title">
							</div>
							<div class="text-9 text-9 t-negro frase_nosotros">
								<?=$nosotros2?>
							</div>
							<div class="uk-padding-remove">
								<hr class="hr-bottom">
							</div>
						</div>
					</div>

					</div>
				</div>


				<div class="uk-width-1-2@m uk-width-1-1 uk-margin-remove uk-padding-remove product movil-1-1 height-100-intro" style="background-image: url(./img/contenido/varios/<?= $rowCONSULTA['imagen4']?>);background-repeat:no-repeat;background-size: cover;">

					<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-grid-collapse">
				        <div class=" uk-padding-remove uk-margin-remove uk-visible@m">
							<div class="uk-padding-remove uk-margin-remove slick-carousel2">
							<?php 
							for ($i=0; $i < sizeof($arrayList); $i++):
								if(($i%2)==0) $claseColor=$rowCONSULTA['color1'];
								else $claseColor=$rowCONSULTA['color2'];

								$existencia=$arrayList[$i]["existencias"];
							?>
								<div class="uk-padding-remove uk-margin-remove" style="height: calc(100vh / 2);
									background-color:<?=$claseColor?>">
									<div style="padding:84px 20px 60px 20px;height: calc(50vh - 148px);">
										<div class="uk-flex uk-flex-center uk-flex-middle" style="height:100%">
											<div class="uk-width-1-3@m uk-padding-small mini" style="width:210px;width:210px;">
												<div style="margin-top:20px;height:190px;width:190px;
													-webkit-box-shadow: -12px -12px 0px 0px rgba(110,190,71,1);
													-moz-box-shadow: -12px -12px 0px 0px rgba(110,190,71,1);
													box-shadow: -12px -12px 0px 0px rgba(110,190,71,1);">
													<a class="uk-width-1-1" href="<?=$arrayList[$i]["id"]?>_productosdetalle">
														<div class="uk-card uk-card-hover uk-padding-remove" style="background:#fff;">
															<div class="uk-card-media-top uk-flex uk-flex-center uk-flex-middle" style="height: 150px;">
												                <img 
												                style="
												                max-height:130px;max-width:130px;" 
												                src="./img/contenido/productos/<?=$arrayList[$i]["imagen"]?>" alt="">
												            </div>
												            <div class="uk-card-title uk-margin-remove uk-text-center"  style="font-size:14px;padding:5px 10px 6px 10px">
												            	<b><?= $arrayList[$i]["titulo"]?></b>
												            	<br>
												            	<?= $arrayList[$i]["precio"]?>
												            </div>
												        </div>
												    </a>
												</div>
											</div>
											<div class="uk-width-2-3@m uk-width-1-1@s uk-width-1-1 uk-padding-small uk-margin-remove prod-detalle mini"
											style="padding-bottom:0!important">
												<div class="name-prod">
													<?= $arrayList[$i]["titulo"] ?>
												</div>
												<div class="uk-width-1-1 uk-padding-remove uk-margin-remove">
													<hr class="hr-detalle">
												</div>
												<div class="uk-width-1-1  uk-padding-remove uk-margin-remove text-9 parrafos descripcion">
													<?= $arrayList[$i]["txt"] ?>
												</div>
												<div class="uk-width-1-1" style="padding:1px 0;">
													<div><hr class="hr-detalle-fin"> </div>
												</div>
												
												<div class="uk-width-1-1" style="
												text-align: right!important;
											    margin: 10px 0 10px 0;
											    color: #fff;
											    border: solid transparent;"> 
													<?= $arrayList[$i]["precio"] ?>
												</div>
												
												<?php if($existencia <= 0):?>
											        <p style="color:tomato;text-align:right;">Producto Agotado </p> 
									            <?php endif ?>
												<?php if($existencia >= 1):?>
												<div style="padding:0;">
													<div uk-grid class="uk-flex uk-flex-right@s uk-flex-center uk-padding-remove uk-margin-remove" style="">
														<div class="uk-width-1-2 uk-button cont-btn uk-grid-collapse uk-padding-remove" uk-grid style="
														float:left;">
															<div class="uk-width-expand uk-button text-cant">
																	Cantidad
															</div>
															<div class="uk-width-auto uk-flex uk-flex-center uk-flex-middle text-7 numero">
																<input class="cantidadNumerica" type="number" min="1" name="" id="<?=$arrayList[$i]['id']?>" placeholder="1" value="1" data-tope="<?= $existencia ?>" style="color:#fff;text-align:center; max-width:40px;">
															</div>
														</div>
														<div class="uk-width-1-2 uk-flex uk-flex-right" style="">
															<div class="uk-button uk-button-personal btn-comprar buybutton" data-id="<?=$arrayList[$i]['id']?>">
																Comprar 
															</div>
														</div>
													</div>
												</div>
												<?php endif ?>
											</div>
										</div>
									</div>
						        </div>
						    <?php endfor ?>
							</div>
				        </div>
				        <div class=" uk-padding-remove uk-margin-remove uk-hidden@m">
							<div class="uk-padding-remove uk-margin-remove slick-carousel1">
							<?php 
							for ($i=0; $i < sizeof($arrayList); $i++):
								if(($i%2)==0) $claseColor=$rowCONSULTA['color1'];
								else $claseColor=$rowCONSULTA['color2'];
							?>
								<div class="uk-padding-remove uk-margin-remove slider-height1" style="
									background-color:<?=$claseColor?>;">
									<div class=" uk-padding-remove pad-height">
										<div class="" style="height:auto;padding:30px 20px;">
											<div class="uk-width-1-1 uk-flex uk-flex-center uk-flex-middle uk-padding mini"style="">
												<div style="margin-top:20px;height:230px;width:230px;
													-webkit-box-shadow: -12px -12px 0px 0px rgba(86,117,8,1);
													-moz-box-shadow: -12px -12px 0px 0px rgba(86,117,8,1);
													box-shadow: -12px -12px 0px 0px rgba(86,117,8,1);">
												<a class="uk-width-1-1" href="<?=$arrayList[$i]["id"]?>_productosdetalle">
													<div class="uk-card uk-card-hover uk-padding-small" style="background:#fff;">
														<div class="uk-card-media-top uk-flex uk-flex-center uk-flex-middle" style="height: 150px;">
											                <img 
											                style="
											                max-height:130px;max-width:130px;" 
											                src="./img/contenido/productos/<?=$arrayList[$i]["imagen"]?>" alt="">
											            </div>

											            <div class="uk-card-title uk-padding-small uk-margin-remove uk-text-center"  style="font-size:14px">
											            	<b><?= $arrayList[$i]["titulo"]?></b>
											            	<br>
												            	<?= $arrayList[$i]["precio"]?>
											            </div>
											        </div>
											    </a>
												</div>
											</div>
											<div class="uk-width-1-1 uk-padding-small uk-margin-remove prod-detalle mini"
											style="padding-bottom:0!important;">
												<div class="name-prod">
													<?= $arrayList[$i]["titulo"] ?>
												</div>
												<div class="uk-width-1-1 uk-padding-remove uk-margin-remove">
													<hr class="hr-detalle">
												</div>
												<div class="uk-width-1-1  uk-padding-remove uk-margin-remove text-9 parrafos descripcion">
													<?= $arrayList[$i]["txt"] ?>
												</div>
												<div class="uk-width-1-1" style="padding:1px 0;">
													<div><hr class="hr-detalle-fin"> </div>
												</div>
												
												<div class="uk-width-1-1" style="
												text-align: right!important;
											    margin: 10px 0 10px 0;
											    color: #fff;
											    border: solid transparent;"> 
													 <b>$</b> <?= $arrayList[$i]["precio"] ?>
												</div>
												
												<div style="padding:20px 0;">
													<div uk-grid class="uk-flex uk-flex-right@s uk-flex-center uk-padding-remove uk-margin-remove" style="">
														<div class="uk-width-1-2 uk-button cont-btn uk-grid-collapse uk-padding-remove width-mini" uk-grid style="
														float:left;">
															<div class="uk-width-expand uk-button text-cant width-medio" style="">
																	Cantidad
															</div>
															<div class="uk-width-auto uk-flex uk-flex-center uk-flex-middle text-7 numero width-medio" style="">
																<input type="number" min="1" name="" id="<?=$arrayList[$i]['id']?>" placeholder="1" value="1" style="color:#fff;text-align:center; max-width:40px;">
															</div>
														</div>
														<div class="uk-width-1-2 uk-flex uk-flex-right width-mini p-m-remove">
															<div class="uk-button uk-button-personal btn-comprar buybutton width-mini" data-id="<?=$arrayList[$i]['id']?>">
																Comprar 
															</div>
														</div>
													</div>
												</div>
												
											</div>
										</div>
									</div>
						        </div>
						    <?php endfor ?>

							</div>
				        </div>
				    </div>

				</div>
				
			</div>
		</div>
	</div>

	<div id="section3" class="section uk-margin-remove uk-padding-remove height-100" 
	style="background-color:#f6f6f6">
		<div class="uk-container uk-container-expand uk-margin-remove uk-padding-remove" id="">
			<div class="uk-grid uk-margin-remove uk-padding-remove" uk-grid>
				<div class="uk-width-1-2@m uk-margin-remove uk-padding uk-flex uk-flex-middle movil-1-1  seccion-padding height-100-intro" style="padding-bottom:0!important">
					<div class="width-mini-contacto uk-margin-remove uk-padding-remove" style="padding-top:76px!important;">
						
						<div class="uk-width-1-1 titulo-g t-negro"> 
							¿Quieres ser distribuidor?
						</div>
						<div class="uk-width-1-1 text-11 t-negro">
							¿Necesitas ayuda o deseas saber algo más sobre nuestros productos? <br>
							Escribenos.<br>
							¡Estamos para servirte!
						</div>

						<div class="uk-width-1-1 uk-padding uk-grid movil-mini-1 p-m-remove" uk-grid style="margin-top:0">
							<div class="uk-width-1-3@m uk-width-1-1 uk-padding-remove  uk-margin-remove uk-padding-remove">
								<input type="text" class="uk-input input-personal uk-padding-remove" id="footernombre" placeholder="Nombre">
							</div>
							<div class="uk-width-1-3@m uk-width-1-1 uk-padding-remove  uk-margin-remove uk-padding-remove">
								<input type="text" class="uk-input input-personal uk-padding-remove" id="footertelefono" placeholder="Whats app">
							</div>
							<div class="uk-width-1-3@m uk-width-1-1 uk-padding-remove uk-margin-remove uk-padding-remove">
								<input type="email" class="uk-input input-personal uk-padding-remove" id="footeremail" placeholder="Correo">
							</div>
							<div class="uk-width-1-1 uk-padding-remove uk-margin-remove">
								<input type="email" class="uk-input input-personal uk-padding-remove" id="footercomentarios" placeholder="Mensaje">
							</div>
							
						</div>
					
						<div class="uk-text-left uk-margin-remove uk-padding-remove movil-pad-30">
							<button class="uk-button uk-button-personal footer-enviar" id="footersend">Enviar</button>
						</div>
						
					</div>
				</div>
				<div class="uk-width-1-2@m uk-margin-remove uk-padding-remove product movil-1-1" style="height:auto!important">
					<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(img/design/ecomme_banner.jpg);background-position:center;background-size: cover;height:100%;width:100%;">
			        </div>
				</div>
			</div>
		</div>
	</div>

	<div id="section4" class="section uk-padding height-100">
		<?=$footer?>
	</div>
</div>




<script type="text/javascript" src="../dist/fullpage.js"></script>
<script type="text/javascript">
	if($(window).width() > 960){
		var myFullpage = new fullpage('#fullpage', {
			sectionsColor: ['#f2f2f2', '#ffffff', '#f6f6f6', 'ffffff', '#ffffff'],
		});
	}
	else{ 
		$('#fullpage').removeAttr('id'); 
	}
</script>


<?=$scriptGNRL?>
<script type="text/javascript">
	$( document ).ready(function() {
		$(".cantidadNumerica").change(function(){
			var tope = $(this).data("tope");
			console.log(tope);
			var cantidad = $(this).val();
			if( cantidad > tope){
				$(this).val(tope);
				alert("Por el momento no contamos con esa coantidad");
			}
		});
	})
</script>

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
	if($(window).width() < 960){

		$('.slick-carousel1').slick({
		  autoplay:false,
		  infinite: true,
		  vertical:false,
		  verticalSwiping:false,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  dots:true,
		  prevArrow: $('.top-arrow'),
		  nextArrow: $('.bottom-arrow')
		});
	}
	else{
		$('.slick-carousel2').slick({
		  autoplay:false,
		  infinite: true,
		  vertical:true,
		  verticalSwiping:true,
		  slidesToShow: 2,
		  slidesToScroll: 2,
		  prevArrow: $('.top-arrow'),
		  nextArrow: $('.bottom-arrow')
		});
	}
</script>

<script>
  // Envío de correo 
  $(document).ready(function() {
  	$("#contactosend").click(function(){
  		var fallo = 0;
  		var nombre = $("#contactonombre").val();
  		var email = $("#contactoemail").val();
  		var telefono = $("#contactotelefono").val();
  		var mensaje = $("#footercomentarios").var();

		var alerta = "";
		
  		
  		$("input").removeClass("uk-form-danger");
  		$("textarea").removeClass("uk-form-danger");


  		if (telefono.length < 8) { fallo=1; alerta="Ingrese telefono valido"; id="contactotelefono"; } 

  		if (email=="") { 
  			fallo=1; alerta="Falta email"; id="contactoemail";
  		}else{
  			var n = email.indexOf("@");
  			var l = email.indexOf(".");
  			if ((n*l)<2) { 
  				fallo=1; alerta="Proporcione un email válido"; id="contactoemail";
  			} 
  		}

  		if (nombre=="") { fallo=1; alerta="Falta nombre"; id="contactonombre"; }

  		var parametros = {
  			"contacto" : 1,
  			"nombre" : nombre,
  			"email" : email,
  			"telefono" : telefono,
  			"mensaje" : mensaje
  		};

  		console.log(parametros);

  		if (fallo == 0) {
  			$.ajax({
  				data:  parametros,
  				url:   "includes/acciones.php",
  				type:  "post",
  				beforeSend: function () {
  					$("#contactosend").html("<div uk-spinner></div>");
  					$("#contactosend").prop("disabled",true);
  					$("#contactosend").disabled = true;
  					UIkit.notification.closeAll();
  					UIkit.notification('<div class="uk-text-center color-blanco bg-blue padding-10 text-lg"><i  uk-spinner></i> Espere...</div>');
  				},
  				success:  function (msj) {
  					$("#contactosend").html("Enviar");
  					$("#contactosend").disabled = false;
  					$("#contactosend").prop("disabled",false);
  					console.log(msj);
  					datos = JSON.parse(msj);
  					UIkit.notification.closeAll();
  					UIkit.notification(datos.msj);
  					if (datos.estatus==0) {
  						$("#contactoasunto").val("");

  						$("#contactonombre").val("");
				  		$("#contactoemail").val("");
				  		$("#contactotelefono").val("");
				  		$("#footercomentarios").val("");
  					}
  				}
  			})
  		}else{
  			UIkit.notification.closeAll();
  			UIkit.notification('<div class="uk-text-center color-blanco bg-danger padding-10 text-lg"><i uk-icon="icon:warning;ratio:2;"></i> &nbsp; '+alerta+'</div>');
  			$("#"+id).focus();
  			$("#"+id).addClass("uk-form-danger");
  		}
  		
  	})
  });

</script>


</body>
</html>