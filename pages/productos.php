<!DOCTYPE html>
<?=$headGNRL?>
<body>
<?php
//PAGINADOR

	$pag=(isset($_GET['pag']))?$_GET['pag']:0;
	$prodInicial= $prodsPagina*$pag;
?>
  
<?=$header?>

<div class="padding-top-100 uk-visible@m"></div>
	<div class="uk-width-1-1 uk-margin-remove uk-padding-remove uk-visible@m">
		<div class="uk-container uk-container-expand uk-margin-remove uk-padding uk-grid" uk-grid>
		<?php

		if(isset($catId)){
			$CONSULTA_vepadre = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $catId");
			$elPadre = $CONSULTA_vepadre -> fetch_assoc();
			$elPadreid = $elPadre['parent'];
		}
		else{
			$elPadreid = 0;
		}
			$CONSULTA_categorias = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0");
			$i = 0;
			$numCat = $CONSULTA_categorias -> num_rows;
			while ($rowCategorias = $CONSULTA_categorias -> fetch_assoc()):

				if(!isset($catId) AND $i == 0){
					$catId = $rowCategorias["id"];
				}
				if($catId == $rowCategorias["id"]){
					$activParent = 'subcategoria-activa';
				}
				else{
					$activParent = '';
				}
				if($elPadreid!=0 && $elPadreid==$rowCategorias["id"]){
					$activParent = 'subcategoria-activa';
				}
				$cats = $rowCategorias["txt"];
				$descripcion = $rowCategorias["descripcion"];
				$i++;
		?>
		<?php if($numCat > 1): ?>
			<div class="uk-width-1-5@m uk-width-1-1 uk-padding-small" style="margin-top:5px;">
				<div class="uk-padding-remove uk-margin-remove uk-grid" uk-grid style="height: auto!important;">
					<a href="<?= $rowCategorias['id'] ?>_productos.php" class="uk-width-1-1 uk-button uk-button-negro uk-text-uppercase uk-text-light <?= $activParent ?>" 
					style="padding-bottom:0!important;margin-bottom:0!important" 
					id="footersend">
						<b><?= $cats ?></b>
					</a>
				</div>
			</div>
		<?php endif ?>
		
	<?php endwhile ?>
	</div>
</div>

<div class=" uk-hidden@m" style="padding:20px;"></div>
	<div class="uk-width-1-1 uk-margin-remove uk-padding uk-hidden@m">
		<div class="uk-container uk-margin-remove uk-padding uk-grid" uk-grid>

		<ul class="uk-width-1-1" uk-accordion>
		    <li class="">
		        <a class="uk-accordion-title uk-text-uppercase" style="color:#000" href="#"><b>Ver Categorias</b></a>
		        <div class="uk-accordion-content" style="">
		    
					<?php
						if(isset($catId)){
			$CONSULTA_vepadre = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $catId");
			$elPadre = $CONSULTA_vepadre -> fetch_assoc();
			$elPadreid = $elPadre['parent'];
		}
		else{
			$elPadreid = 0;
		}
			$CONSULTA_categorias = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0");
			$i = 0;
			$numCat = $CONSULTA_categorias -> num_rows;
			while ($rowCategorias = $CONSULTA_categorias -> fetch_assoc()):

				if(!isset($catId) AND $i == 0){
					$catId = $rowCategorias["id"];
				}
				if($catId == $rowCategorias["id"]){
					$activParent = 'subcategoria-activa';
				}
				else{
					$activParent = '';
				}
				if($elPadreid!=0 && $elPadreid==$rowCategorias["id"]){
					$activParent = 'subcategoria-activa';
				}
				$cats = $rowCategorias["txt"];
				$descripcion = $rowCategorias["descripcion"];
				$i++;
		?>
					
					<?php if($numCat > 1): ?>

				
					<div class="uk-margin-remove uk-grid" uk-grid style="height: auto!important;padding:10px 0;">
						<a href="<?= $rowCategorias["id"] ?>_productos.php" class="uk-width-1-1 uk-text-uppercase uk-text-light" 
						style="padding-bottom:0!important;margin-bottom:0!important" 
						id="footersend">
							<span uk-icon="icon: triangle-right; ratio:1"></span><b><?= $cats ?></b>
						</a>
					</div>
				
					<?php endif ?>
			
					<?php endwhile ?>
				</div>
			</li>
    
		</ul>
	</div>
</div>



<?php
	
	$CONSULTA_cat = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $catId LIMIT 1");
	$rowCat = $CONSULTA_cat -> fetch_assoc();
	$nombreCat = $rowCat['txt'];
	$descripcionCat = $rowCat['descripcion'];
	$cantProducts=0;
	$arrayList = array();
	$subId = $catId;
	
	/*Este es para saber si tiene hijos y desplegar la primera subcategoria*/
	if($rowCat['parent'] == 0){
		$CONSULTA_subcatPrimera = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = $catId ORDER BY orden LIMIT 1");
		$primera = $CONSULTA_subcatPrimera -> fetch_assoc();
		$subId = $primera['id'];
		
	}
	else{
		$catId = $rowCat['parent'];
		$CONSULTA_cat2 = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $catId LIMIT 1");
		$rowhijo = $CONSULTA_cat2 -> fetch_assoc();
		$nombreCat = $rowhijo['txt'];
		$descripcionCat = $rowhijo['descripcion'];
	}
		$CONSULTA_subcat = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $subId");
		$rowSubCat = $CONSULTA_subcat -> fetch_assoc();
			$categoria = $rowSubCat['id'];
			/*Esta consulta es solo para el paginador*/
			$consultaProd = $CONEXION -> query("SELECT * FROM productos WHERE categoria = $categoria AND estatus = 1");
			$cantProducts = $consultaProd -> num_rows;

			$consultaProductos = $CONEXION -> query("SELECT * FROM productos WHERE categoria = $categoria AND estatus = 1 LIMIT $prodInicial, $prodsPagina");
			
			while($rowProductos = $consultaProductos -> fetch_assoc()){

				$prodId = $rowProductos["id"];
				$precio='';

			    $preciocampo = 'precio';

				$descuento = $rowProductos['descuento'];

				if(isset($ulevel)){
					$precio='';
					switch ($ulevel) {
						case '0':
							$preciocampo = 'precio';
							break;
						case '1':
							$preciocampo = 'precio1';
							break;
						case '2':
							$preciocampo = 'precio2';
							break;
						case '3':
							$preciocampo = 'precio3';
							break;
						
						default:
							# code...
							break;
					}

				}
				$precio = ($descuento>0)?'
			      <strike style="font-weight: 600;">$'.number_format(($rowProductos[$preciocampo]),2).'</strike><span> / </span><span>$'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2).'</span>
			      ':'
			      $'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2);
			      
			      $rowProductos['precio']=$precio;

				$consultaPic = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $prodId  ORDER BY orden LIMIT 1");
				if($consultaPic -> num_rows > 0)
				{
					$pic = $consultaPic -> fetch_assoc();
					$picImg = $pic["id"].".jpg";
					$rowProductos["imagen"] = $picImg;
				}
				else{
					$picImg = "";
				}
				array_push($arrayList, $rowProductos);
			}	
?>


			


	<div class="uk-container uk-container-expand">
			<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-grid" uk-grid>
				<div class="uk-width-1-1 uk-margin-remove uk-padding t-verde text-xxxl padding-h-40">
					<?= $nombreCat ?>
				</div>
				<div class="uk-width-1-1 uk-margin-remove padding-h-40">
					<hr class="ht-prods">
				</div>
				<div class="uk-width-1-1 uk-margin-remove uk-padding t-verde padding-h-40" style="padding-bottom:50px">
					<?= $descripcionCat ?> 
				</div>
					<div class="uk-width-1-1 uk-margin-remove" style="padding:0 60px">
					<div class="uk-container uk-container-small"> 
						<div class="uk-margin-remove uk-padding-remove uk-grid" uk-grid>
						<?php

							$CONSULTA_categorias = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = $catId");
							$i = 0;
							$numCat = $CONSULTA_categorias -> num_rows;

							while ($rowCategorias = $CONSULTA_categorias -> fetch_assoc()):
								
								if(!isset($catId) AND $i == 0){
									$catId = $rowCategorias["id"];
								}
								if($subId  == $rowCategorias['id']){
									$subcatActiva = 'subcategoria-activa';	
								}else {
									$subcatActiva = "";
								}

								$cats = $rowCategorias["txt"];
								$descripcion = $rowCategorias["descripcion"];
								$i++;
						?>
						<?php if($numCat > 0): ?>
							<!-- #4d6b05 -->
							<div class="uk-width-1-4@m uk-width-1-1 uk-padding-small" style="margin-top:5px;">
								<div class="uk-padding-remove uk-margin-remove uk-grid" uk-grid style="height: auto!important;">
									<a href="<?= $rowCategorias["id"] ?>_productos.php" class="uk-width-1-1 uk-button uk-button-negro uk-text-uppercase uk-text-light buybutton <?= $subcatActiva ?>" data-id="<?=$arrayList[$i]['id']?>" 
									style="padding-bottom:0!important;margin-bottom:0!important" 
									id="footersend">
										<b><?= $cats ?></b>
									</a>
								</div>
							</div>
						<?php endif ?>
						
					<?php endwhile ?>
					</div>
					</div>
				</div>
			</div>

			<div uk-grid class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-child-width-1-5@xl uk-grid-medium" style="margin-left: 0px; padding-left: 0px;">
			<?php 
				for($i = 0; sizeof($arrayList) > $i; $i++):
					$prodId = $arrayList[$i]["id"];
			?>
				<div class="pad-movil-10" >
					<div class=" uk-flex uk-flex-center">							
						<div style="width:250px;
							-webkit-box-shadow: -12px -12px 0px 0px rgba(92,178,49,1);
							-moz-box-shadow: -12px -12px 0px 0px rgba(92,178,49,1);
							box-shadow: -12px -12px 0px 0px rgba(92,178,49,1);">
							<a class="uk-width-1-1 uk-margin-remove uk-padding-remove" 
							href="<?=$prodId ?>_productosdetalle">
								<div class="uk-card uk-card-hover sombra" style="background:#fff;padding:20px; height:250px; width:250px">
									<div class="uk-card-media-top uk-flex uk-flex-center uk-flex-middle" style="height: 150px;">
						                <img 
						                style="
						                max-height: 150px;max-width:150px;" 
						                src="./img/contenido/productos/<?=$arrayList[$i]["imagen"]?>" alt="">
						            </div>
						            <div class="uk-card-title uk-margin-remove uk-text-center"  style="font-size:14px;padding:4px;padding-top: 10px">
						            	<b><?= $arrayList[$i]["titulo"]?></b>
						            </div>
						            
						            <div class="uk-card-title uk-margin-remove uk-text-center"  style="font-size:14px;padding:4px;padding-bottom: 10px">
						            	<?= $arrayList[$i]["precio"]?>
						            </div>
						            
						        </div>
						    </a>
						</div>
					</div>		
					<div class="uk-flex uk-flex-center">
						<div class="uk-width-2-3@m uk-width-1-1 uk-padding-small" style="margin-top:5px;">
							<div class="uk-padding-remove uk-margin-remove uk-grid" uk-grid style="height: auto!important;">
								<a href="<?=$prodId ?>_productosdetalle" class="uk-width-1-1 uk-button uk-button-negro uk-text-uppercase uk-text-light <?= $activParent ?>" 
								style="padding-bottom:0!important;margin-bottom:0!important;font-size:12px" 
								id="footersend">
									<b>Ver Detalle</b>
								</a>
							</div>
						</div>
					</div>
						
				</div>
			<?php endfor ?>
				<!-- PAGINATION -->
				<div class="bg-bottom uk-width-1-1" >
				  <div class="padding-v-50">
				    <ul class="uk-pagination uk-flex-center uk-text-center">
				      <?php
				      
				      if ($pag!=0) {
				        $link=$catId.'_'.($pag-1).'_productos.html';
				        echo'
				        <li><a href="'.$link.'" class="pagination-arrows"><i class="fa fa-lg fa-angle-left"></i> &nbsp;&nbsp; Anterior</a></li>';
				      }
				      $pagTotal=intval($cantProducts/$prodsPagina);
				      $modulo=$cantProducts % $prodsPagina;
				      if (($modulo) == 0){
				        $pagTotal=($cantProducts/$prodsPagina)-1;
				      }
				      for ($i=0; $i <= $pagTotal; $i++) { 
				        $clase='';
				        if ($pag==$i) {
				          $clase='uk-active';
				        }
				        $link=$catId.'_'.$i.'_productos.html';
				        echo '<li><a href="'.$link.'" class="'.$clase.'">'.($i+1).'</a></li>';
				      }
				      if ($pag!=$pagTotal AND $cantProducts!=0) {
				        $link=$catId.'_'.($pag+1).'_productos.html';
				        echo'
				        <li><a href="'.$link.'" class="pagination-arrows">Siguiente &nbsp;&nbsp; <i class="fa fa-lg fa-angle-right"></i></a></li>
				        ';
				      }
				      ?>
				    </ul>
				  </div>
				</div><!-- PAGINATION -->
			</div>

			
		
	</div>



<?=$footer?>

<?=$scriptGNRL?>
<script type="text/javascript">
	$( document ).ready(function() {
		var tope = <?= $existencia?>;
		$(".cantidadNumerica").change(function(){
			var cantidad = $(this).val();
			if( cantidad > tope){
				$(this).val(tope);
				alert("Por el momento no contamos con esa coantidad");
			}
		});
	})
</script>

</body>
</html>