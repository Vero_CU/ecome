<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
	
	<div id="section3" class="section uk-margin-remove uk-padding-remove height-100" 
	style="background-color:#f6f6f6">
		<div class="uk-container uk-container-expand uk-margin-remove uk-padding-remove" id="">
			<div class="uk-grid uk-margin-remove uk-padding-remove" uk-grid>
				<div class="uk-width-1-2@m uk-margin-remove uk-padding uk-flex uk-flex-middle movil-1-1  seccion-padding height-100-intro" style="padding-bottom:0!important">
					<div class="width-mini-contacto uk-margin-remove uk-padding-remove" style="padding-top:76px!important;">
						
						<div class="uk-width-1-1 titulo-g t-negro"> 
							¿Quieres ser distribuidor?
						</div>
						<div class="uk-width-1-1 text-11 t-negro">
							¿Necesitas ayuda o deseas saber algo más sobre nuestros productos? <br>
							Escribenos.<br>
							¡Estamos para servirte!
						</div>

						<div class="uk-width-1-1 uk-padding uk-grid movil-mini-1 p-m-remove" uk-grid style="margin-top:0">
							<div class="uk-width-1-3@m uk-width-1-1 uk-padding-remove  uk-margin-remove uk-padding-remove">
								<input type="text" class="uk-input input-personal uk-padding-remove" id="footernombre" placeholder="Nombre">
							</div>
							<div class="uk-width-1-3@m uk-width-1-1 uk-padding-remove  uk-margin-remove uk-padding-remove">
								<input type="text" class="uk-input input-personal uk-padding-remove" id="footertelefono" placeholder="Whats app">
							</div>
							<div class="uk-width-1-3@m uk-width-1-1 uk-padding-remove uk-margin-remove uk-padding-remove">
								<input type="email" class="uk-input input-personal uk-padding-remove" id="footeremail" placeholder="Correo">
							</div>
							<div class="uk-width-1-1 uk-padding-remove uk-margin-remove">
								<input type="email" class="uk-input input-personal uk-padding-remove" id="footercomentarios" placeholder="Mensaje">
							</div>
							
						</div>
					
						<div class="uk-text-left uk-margin-remove uk-padding-remove movil-pad-30">
							<button class="uk-button uk-button-personal footer-enviar" id="footersend">Enviar</button>
						</div>
						
					</div>
				</div>
				<div class="uk-width-1-2@m uk-margin-remove uk-padding-remove product movil-1-1" style="height:auto!important">
					<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(img/design/ecomme_banner.jpg);background-position:center;background-size: cover;height:100%;width:100%;">
			        </div>
				</div>
			</div>
		</div>
	</div>

<div class="margin-top-50"></div>
<?=$footer?>

<?=$scriptGNRL?>

<script>
  // Envío de correo 
  $(document).ready(function() {
  	$("#contactosend").click(function(){
  		var fallo = 0;
  		var nombre = $("#contactonombre").val();
  		var email = $("#contactoemail").val();
  		var telefono = $("#contactotelefono").val();
  		var mensaje = $("#footercomentarios").var();

		var alerta = "";
		
  		
  		$("input").removeClass("uk-form-danger");
  		$("textarea").removeClass("uk-form-danger");


  		if (telefono.length < 8) { fallo=1; alerta="Ingrese telefono valido"; id="contactotelefono"; } 

  		if (email=="") { 
  			fallo=1; alerta="Falta email"; id="contactoemail";
  		}else{
  			var n = email.indexOf("@");
  			var l = email.indexOf(".");
  			if ((n*l)<2) { 
  				fallo=1; alerta="Proporcione un email válido"; id="contactoemail";
  			} 
  		}

  		if (nombre=="") { fallo=1; alerta="Falta nombre"; id="contactonombre"; }

  		var parametros = {
  			"contacto" : 1,
  			"nombre" : nombre,
  			"email" : email,
  			"telefono" : telefono,
  			"mensaje" : mensaje
  		};

  		console.log(parametros);

  		if (fallo == 0) {
  			$.ajax({
  				data:  parametros,
  				url:   "includes/acciones.php",
  				type:  "post",
  				beforeSend: function () {
  					$("#contactosend").html("<div uk-spinner></div>");
  					$("#contactosend").prop("disabled",true);
  					$("#contactosend").disabled = true;
  					UIkit.notification.closeAll();
  					UIkit.notification('<div class="uk-text-center color-blanco bg-blue padding-10 text-lg"><i  uk-spinner></i> Espere...</div>');
  				},
  				success:  function (msj) {
  					$("#contactosend").html("Enviar");
  					$("#contactosend").disabled = false;
  					$("#contactosend").prop("disabled",false);
  					console.log(msj);
  					datos = JSON.parse(msj);
  					UIkit.notification.closeAll();
  					UIkit.notification(datos.msj);
  					if (datos.estatus==0) {
  						$("#contactoasunto").val("");

  						$("#contactonombre").val("");
				  		$("#contactoemail").val("");
				  		$("#contactotelefono").val("");
				  		$("#footercomentarios").val("");
  					}
  				}
  			})
  		}else{
  			UIkit.notification.closeAll();
  			UIkit.notification('<div class="uk-text-center color-blanco bg-danger padding-10 text-lg"><i uk-icon="icon:warning;ratio:2;"></i> &nbsp; '+alerta+'</div>');
  			$("#"+id).focus();
  			$("#"+id).addClass("uk-form-danger");
  		}
  		
  	})
  });

</script>

</body>
</html>