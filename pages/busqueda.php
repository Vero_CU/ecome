<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<?php
	$arrayList=array();
	$consultaProductos = $CONEXION -> query("SELECT * FROM productos WHERE estatus = 1 AND titulo LIKE  '%$palabra%'");
	while($rowProductos = $consultaProductos -> fetch_assoc()){
		$prodId = $rowProductos["id"];
		
		$precio='';

	    $preciocampo = 'precio';

		$descuento = $rowProductos['descuento'];

		if(isset($ulevel)){
			$precio='';
			switch ($ulevel) {
				case '0':
					$preciocampo = 'precio';
					break;
				case '1':
					$preciocampo = 'precio1';
					break;
				case '2':
					$preciocampo = 'precio2';
					break;
				case '3':
					$preciocampo = 'precio3';
					break;
				
				default:
					# code...
					break;
			}

		}
		$precio = ($descuento>0)?'
	      <strike style="font-weight: 600;">$'.number_format(($rowProductos[$preciocampo]),2).'</strike><span> / </span><span>$'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2).'</span>
	      ':'
	      $'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2);

	      $rowProductos['precio']=$precio;

		$consultaPic = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $prodId  ORDER BY orden LIMIT 1");
		$pic = $consultaPic -> fetch_assoc();
		$picImg = $pic["id"].".jpg";
		$rowProductos["imagen"] = $picImg;
		array_push($arrayList, $rowProductos);
	}
?>

<div class="padding-top-100"></div>
		<div class="uk-container uk-container-expand padding-top-100">
			
			<div uk-grid class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-child-width-1-5@xl uk-grid-medium" style="margin-left: 0px; padding-left: 0px;">
				<?php 
					for($i = 0; sizeof($arrayList) > $i; $i++):
						$prodId = $arrayList[$i]["id"];
				?>
				<div class="pad-movil-10" >
					<div class=" uk-flex uk-flex-center">							
						<div style="width:250px;
							-webkit-box-shadow: -12px -12px 0px 0px rgba(86,117,8,1);
							-moz-box-shadow: -12px -12px 0px 0px rgba(86,117,8,1);
							box-shadow: -12px -12px 0px 0px rgba(86,117,8,1);">
							<a class="uk-width-1-1 uk-margin-remove uk-padding-remove" 
							href="<?=$prodId ?>_productosdetalle">
								<div class="uk-card uk-card-hover sombra" style="background:#fff;padding:20px; height:250px; width:250px">
									<div class="uk-card-media-top uk-flex uk-flex-center uk-flex-middle" style="height: 150px;">
						                <img 
						                style="
						                max-height: 150px;max-width:150px;" 
						                src="./img/contenido/productos/<?=$arrayList[$i]["imagen"]?>" alt="">
						            </div>
						            <div class="uk-card-title uk-margin-remove uk-text-center"  style="font-size:14px;padding:4px;padding-top: 10px">
						            	<b><?= $arrayList[$i]["titulo"]?></b>
						            </div>
						            <div class="uk-card-title uk-margin-remove uk-text-center"  style="font-size:14px;padding:4px;padding-bottom: 10px">
						            	<?= $arrayList[$i]["precio"]?>
						            </div>
						        </div>
						    </a>
						</div>
					</div>		
					<div class="uk-width-1-1 uk-flex uk-flex-center padding-top-20" style=";">
						<br>
						<div> 
							<input type="hidden" id="<?=$prodId?>" value="1">
						</div>
						<button class="agregar-carro uk-button uk-button-negro uk-text-uppercase uk-text-light buybutton" data-id="<?=$arrayList[$i]['id']?>" id="footersend">COMPRA AHORA</button>
					</div>
				</div>
			
			<?php endfor ?>
		</div>
		


<?=$footer?>

<?=$scriptGNRL?>

</body>
</html>