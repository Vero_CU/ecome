<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>

<?php 

	$arrayList = array();
	$consultaTestimonios = $CONEXION -> query("SELECT * FROM testimonios");
//sdebug($_SESSION['carro']);
?>


<div class="padding-top-100"></div>
<div class="uk-width-1-1 uk-margin-remove uk-padding-remove">
	<div class="uk-container uk-padding-remove">
		<div class="uk-grid uk-margin-remove uk-padding-remove uk-flex uk-flex-center" uk-grid>

			<div class="uk-width-auto uk-flex uk-flex-center uk-padding uk-grid margin-top-20" uk-grid style="margin-left:0; height:400px;width:440px">
				<div class="cont-prod uk-flex uk-flex-center uk-flex-middle  margin-top-20" style="padding-left: 0px;">
					<div uk-slider class="sombra" style="height: 100%;width:100%">
    					<div class="uk-position-relative uk-visible-toggle uk-flex uk-flex-center uk-flex-middle" tabindex="-1" style="height: 100%;width:100%">
    						<ul class="uk-slider-items uk-flex uk-flex-center uk-flex-middle" style="width: 318px;height: 318px;">
					<?php  
						$consultaProductos = $CONEXION -> query("SELECT * FROM productos WHERE estatus = 1 AND id = $prodId");
						$rowProductos = $consultaProductos -> fetch_assoc();
						 $preciocampo = 'precio';

						$descuento = $rowProductos['descuento'];

						if(isset($ulevel)){
							$precio='';
							switch ($ulevel) {
								case '0':
									$preciocampo = 'precio';
									break;
								case '1':
									$preciocampo = 'precio1';
									break;
								case '2':
									$preciocampo = 'precio2';
									break;
								case '3':
									$preciocampo = 'precio3';
									break;
								
								default:
									# code...
									break;
							}

						}
						$precio = ($descuento>0)?'
					      <strike style="font-weight: 600;">$'.number_format(($rowProductos[$preciocampo]),2).'</strike><span> / </span><span>$'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2).'</span>
					      ':'
					      $'.number_format(($rowProductos[$preciocampo]*(100-$descuento)/100),2);
					      //$existencia=$rowProductos["existencias"];
					      $rowProductos['precio']=$precio;
										
						$consultaPic = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $prodId  ORDER BY orden");
						while ($pic = $consultaPic -> fetch_assoc()) {
							$picImg = $pic["id"].".jpg";
							$rowProductos["imagen"] = $picImg;
							echo '
	    						<li class="" tabindex="0" style="width: 100%; max-width: 318px;">
	    							<div class="uk-flex uk-flex-center uk-flex-middle padding-20">
										<img src="img/contenido/productos/'.$picImg.'" alt="" style="max-height:250px;">
						 			</div>
						        </li>';
						}
					?>
						    </ul>
						    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
	        				<a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
							<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin uk-position-bottom"></ul>
						</div>
					</div>
				</div>
			</div>

			<div class="uk-width-expand@m uk-width-1-1 uk-padding cero-middle">
				<div class="uk-width-1-1">
					<div class="uk-width-1-1 uk-padding-remove uk-margin-remove uk-background-cover uk-flex uk-flex-center uk-flex-middle product uk-grid" uk-grid style="background-image: url(./img/design/banner.jpg);background-repeat:no-repeat;height:100%;">
						<div class="uk-width-1-1 uk-margin-remove uk-padding-remove uk-flex uk-flex-center uk-flex-middle  pad-detalle" style="background-color:<?= $rowCONSULTA['color1']?>;">
							<div class="uk-width-1-1 uk-margin-remove uk-padding uk-flex uk-flex-center uk-flex-middle texto-prod uk-grid" uk-grid>
								<div class="uk-width-1-1 uk-padding-remove uk-margin-remove text-xxl t-blanco">
									<?= $rowProductos["titulo"] ?>
								</div>
								<div class="uk-width-1-1 uk-padding-remove uk-margin-remove">
									<hr class="hr-blanca">
								</div>
								<div class="uk-width-1-1 uk-padding-small uk-margin-remove text-9 t-blanco cero-middle " style="text-align: justify; min-height:250px;">
									<?= $rowProductos["txt"] ?>
								</div>
								<div class="uk-width-1-1 uk-padding-remove uk-margin-remove" >
									<hr class="hr-texto">
								</div>
								<!-- <div class="uk-width-1-1 uk-padding-remove uk-margin-remove">
									<div class="uk-width-1-4@m uk-margin-remove uk-text-center uk-align-right@s precio">
										<?= $rowProductos["precio"] ?>
									</div>
								</div> -->
							</div>
						</div>
					</div>

					<!--<div class="" style="padding:10px 0;">
						<div uk-grid class="uk-flex uk-flex-right@s uk-flex-center uk-padding-remove uk-margin-remove" style="">
							<div class="uk-width-1-2 uk-button cont-btn uk-grid-collapse uk-padding-remove width-mini" uk-grid style="
							float:left;">
								<div class="uk-width-expand uk-button text-cant width-medio" style="">
										Cantidad
								</div>
								<div class="uk-width-auto uk-flex uk-flex-center uk-flex-middle text-7 numero width-medio" style="">
									<input type="number" min="1" name="" id="<?=$arrayList[$i]['id']?>" placeholder="1" value="1" style="color:#fff;text-align:center; max-width:40px;">
								</div>
							</div>
							<div class="uk-width-1-2 uk-flex uk-flex-right width-mini p-m-remove">
								<div class="uk-button uk-button-personal btn-comprar buybutton width-mini" data-id="<?=$rowProductos['id']?>">
									Comprar 
								</div>
							</div>
							<button class="uk-width-1-1 uk-text-center uk-link-reset buybutton uk-text-uppercase"  style="background-color:#fff;border: solid 1px #009bdb;padding:0;marging:0;text-align:center;color: #009bdb;font-size:16px;height:42px" data-id="<?=$rowCONSULTA['id']?>"> Añadir al carrito </button>
						</div>
					</div>-->
				</div>
			</div>

			<div class="uk-width-1-1 uk-grid-small" uk-grid>
				<h1 class="uk-width-1-1">Selecciona el color de tu elección</h1>
				<?php 
				$idProd = $rowProductos['id'];
				$idColors = "";
				$existencias = 0;
				$CONSULTA_has = $CONEXION -> query("SELECT * FROM productos_has_color WHERE id_producto = $idProd AND existencias > 0");
				while ($row_has = $CONSULTA_has -> fetch_assoc()) {
					$idColors = $row_has['id_color'];
					$idProdsRel =$row_has['id'];
					$CONSULTA_color = $CONEXION -> query("SELECT * FROM productoscolor WHERE id = $idColors");
					$idColores = "";
					while ($row_color = $CONSULTA_color -> fetch_assoc()) {
						$idColores = $row_color['id'];
						$existencias = $row_has['existencias'];
						echo '
						<div class="uk-width-1-1 uk-width-1-3@s uk-width-1-5@m uk-margin-remove" style="padding:6px"> 
							<div class="uk-width-1-1 colores" data-id="'.$idColores.'" id="color_'.$idColores.'">
								<a style="cursor:pointer;">
									<div class="uk-width-1-1 uk-card uk-card-default uk-card-body uk-grid-small uk-margin-remove" uk-grid style="padding:6px;border:solid 1px #eee">
										<div class="uk-width-1-1" style="font-size:12px;padding:2px 0;padding-left:0!important;">Contamos con:<br> <strong style="text-decoration: underline;"> '.$existencias.' </strong> en existencia </div>
										<div class="uk-width-auto uk-margin-remove" style="padding-left:0!important;">
											<div style="border: solid 1px;
										    border-radius: 100%;
										    width: 20px;
										    height:20px;
										    padding:3px;
										    
											background:'.$row_color["txt"].';">  </div>
										</div>
										<div class="uk-width-expand uk-text-center uk-flex uk-flex-middle uk-margin-remove" style="font-size:14px;padding:3px">
											'.$row_color["name"].'
										</div>
									</div>
								</a>

								<div class="uk-width-1-1 comprar"  id="comprar_'.$idColores.'">
									<div class="uk-width-1-1 uk-text-left" style="font-size:14px;padding:5px 0">
									 	<div class="uk-width-1-1 uk-grid-small uk-margin-remove" uk-grid style="padding:5px 0">
										 	<div class="uk-width-2-3" style="border:solid transparent; border-top: solid 1px #71c447;border-bottom: solid 1px #71c447;padding:6px 0;text-align:left;color: #fff;background-color: #71c447;font-size:14px;padding-left:10px" class="uk-text-uppercase">
												Cantidad
											</div>
											<input class="uk-width-1-3 cantidadNumerica" type="number" min="0"  max="<?= $existencia ?>" name="" id="'.$idProdsRel.'" placeholder="0" value="0" style="border:solid transparent;border-top: solid 1px #71c447;border-bottom: solid 1px #71c447;padding:6px 0;text-align:left;color: #fff;background-color: #99d57b;font-size:14px;font-weight:500">
											<div class="uk-width-1-1 uk-padding-remove uk-grid" id="lasExistencias" uk-grid style="margin:4px;margin-top:10px;">
												<a class="uk-width-1-1 uk-button uk-button-negro uk-text-uppercase uk-text-light  buybutton" style="padding-bottom:0!important;margin-bottom:0!important;font-size:12px;height: 30px;line-height: 30px;" 
												 data-id="'.$prodId.'" data-existencias="'.$idProdsRel.'">
													<b>Agregar al carrito</b>
												</a>
											</div>
					
									 	</div>
									</div>
							
									
								</div>
							</div>
							
						</div>

						';
					}
				}
				?>
					
			</div>
		</div>
	</div>
</div>


<div class="uk-container uk-container-expand">
	<hr style="margin:50px 0">
	<div class="uk-width-1-1 t-verde text-xxxl" 
	style="color:#000;font-weight:100; padding:0 60px">
		<!-- PRODUCTOS RELACIONADOS -->
	</div>
	<div class="uk-width-1-1 uk-margin-remove" style=" padding:0 60px">
		<hr class="ht-prods" style="">
	</div>
</div>

<?=$footer?>


<?=$scriptGNRL?>
<script type="text/javascript">
	$( document ).ready(function() {
		
		var id = "";
		var comId = "";
		$('.comprar').hide();
		$(".colores").click(function(){
		    id = $(this).attr('id');
		    var comprar = $(this).data("id");
    		$("#comprar_"+comprar).show();
		});

		var tope = <?= $existencias?>;
		console.log("hay", tope);
		$(".cantidadNumerica").change(function(){
			var cantidad = $(this).val();
			if( cantidad > tope){
				$(this).val(tope);
				alert("Por el momento no contamos con esa coantidad");
			}
		});
		
		

	})
	
</script>
</body>
</html>